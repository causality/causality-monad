module Key = struct
  type 'a t =
    {loc: 'a Location.t; join: 'a -> 'a -> 'a option; show: 'a -> string}

  let compare k k' = Location.compare k.loc k'.loc
end

module Value = struct
  type ('a, 'r) t = 'a
end

module PM = PolyMap.MakeFunc (Key) (Value)

type 'a id = 'a Key.t

type t = unit PM.t

let create_id ?(show = fun _ -> "") ~join () =
  {Key.join; show; loc= Location.create ()}

let insert ?show ~join init world =
  let key = create_id ?show ~join () in
  (PM.add key init world, key)

let query key f world = PM.update key f world

let get_opt key world = try Some (PM.lookup key world) with _ -> None

let get ~default key world = try PM.lookup key world with _ -> default

let empty = PM.empty

let show world =
  let show_binding (PM.B (key, value)) = key.show value in
  String.concat "; " @@ List.map show_binding @@ PM.bindings world

let join w1 w2 =
  let merge key x y = key.Key.join x y in
  PM.merge {merge} w1 w2

let compatible w1 w2 = match join w1 w2 with None -> false | Some _ -> true
