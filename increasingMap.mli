(** Representing increasing maps on {!Domain.t} *)

(** In {!Domain}, we have defined the domain of states, which are sets of
    messages. Now, we will see how to interpret computations as
    (parametrised) increasing map over {!Domain.t}, ie. maps that satisfy $d
    \subseteq f(d)$, ie. the state can only grow. Intuitively, $f$ looks at
    messages in the current state and reacts to them by adding new messages.

    Because increasing maps are not necessarily closed, iterating the same
    function over a state $d$ may lead to bigger and bigger states. Thus, to
    understand the final state reached by a program [f], we need not to
    compute [f(\bot)] but the closure of [f] on [\bot], closure obtained by
    iterating [f] until the state is stabilised.

    Iterating [f] naively may lead to lots of useless recomputation and we
    thus use a somewhat smarter representation of such maps which allows to
    speed up a bit the fixpoint calculation. *)

(** The type of increasing maps on {!Domain.t} *)
type t

val empty : t
(** The "empty" increasing map, ie. the identity *)

val is_empty : t -> bool
(** Tests whether an increasing map is {!empty} *)

val dump : t -> string
(** Provide a string representation of an increasing map (for debugging
    purposes) *)

val ( +++ ) : t -> t -> t
(** Componentwise union of increasing maps. *)

val union : t list -> t
(** Union of a list of increasing maps *)

val emit : 'a Domain.Message.t -> t
(** The increasing map that adds a particular message to the input state (if
    not already present) *)

val watch : 'a Location.t -> ('a Domain.Message.t -> t option) -> t
(** Given a location [loc] and a continuation [cont], [watch loc cont] looks
    at which messages in the input states are at location [loc], and for each
    such message [m], spawn a new thread [cont m], in parallel. [cont] may
    return None, in case the thread is not spawned, which can be used when
    the message is viewed as of no interest to the continuation. This is used
    in {!Computation} when the world that emitted the message is not
    compatible with the world that wants to receive it. *)

val init : t -> Domain.t
(** [init f] returns f(\bot) *)

(** {1 Computing closure} *)

(** We now show a few primitives to compute closures of increasing map. We
    are not interested in computing the complete closure of an increasing map
    [f] here (ie. for any domain element [d], the least fixpoint of [f]
    containing [d]), but rather simply, the fixpoint at the empty domain
    element. *)

val close : t -> t
(** [close f] returns the "partial closure" of [f], which entails the
    following. Starting from the empty domain element, it runs the handlers
    of [f] on the messages produced at the beginning by [f], generating new
    messages and handlers, and feed them back into the system until a
    fixpoint is reached.

    Instead of returning only the domain element reached, it returns the
    whole map reached, which also contains information about the handlers set
    up during the computation. This information is crucial to be able to
    compose this with another increasing map that comes later. *)

val add_and_close : t -> t -> t
(** [add_and_close] expects two increasing maps [t] and [u]. The first one is
    supposed to be closed (ie. went through close). The second one does not
    have to be closed. What it does is that it computes the closure of
    [t ∥ u], by avoiding repetition of computing the closure of [t]. So
    messages of [t] are sent to [u], and messages of [u] are sent to
    [t ∥ u]. *)
