module type Signature = sig
  (** An address is a description of an initial cell of the universal arena.
      We assume that the arena is recursive and that for any minimal e, G/e
      ≅ G, ie. the type of address remains valid deep inside G. *)
  type address

  (** Type of data carried by a neutral event. *)
  type neutral

  (** Type of data carried by visible moves of the game G. *)
  type visible

  (** The type of different players. Typically, there are two players, but
      nothing forces this. *)
  module Player : Set.OrderedType
end

(** This functor takes a signature, and a implementation of of the monad, and
    returns a new implementation of the monad which supports playing and
    receiving moves, as well as basic primitives. *)

module Make (Sig : Signature) = struct
  module Label = struct
    type visible_desc =
      { pointer: visible option
            (** A pointer, which is an optional visible move *)
      ; visible: Sig.visible
            (** The content of the move, according to {!Sig} *)
      ; address: Sig.address
            (** Where the move was played in G/pointer (or G if pointer is
                None) *) }

    and neutral_desc = Sig.neutral

    (** Type of the labels of the events. It is parametrised by ['a] which
        represents the kind of event: if 'a = `Visible, then this is a
        visible move played on the universal game G, if 'a = `Neutral, then
        it is a neutral event. *)
    and +'desc _t =
      { id: int  (** Id of the move. *)
      ; desc: 'desc  (** Description of the label *)
      ; player: Sig.Player.t  (** Player who emitted the move *) }

    (** Description of moves *)

    and visible = [`Visible of visible_desc] _t

    and neutral = [`Neutral of neutral_desc] _t

    and t = [`Visible of visible_desc | `Neutral of neutral_desc] _t

    let player {player; _} = player

    let address {desc= `Visible {address; _}; _} = address

    let pointer {desc= `Visible {pointer; _}; _} = pointer

    let data {desc= `Visible {visible; _}; _} = visible

    type label = t

    let id = Some (fun m -> m.id)
  end

  module type GamingMonad = sig
    include Monad.T

    val play :
         ?pointer:Label.visible
      -> Sig.Player.t
      -> Sig.address
      -> Sig.visible
      -> Label.visible t
    (** [play ?pointer player data] plays a new move whose pointer is
        [pointer], played by [player] and with data [data], at [address] *)

    val wait : ?pointer:Label.visible -> Sig.address -> Label.visible t
    (** [wait ?pointer address] waits for a successor to [pointer] (or
        minimal movei f [pointer] is not defined, at address [address] *)

    val neutral : Sig.Player.t -> Sig.neutral -> Label.neutral t
    (** [neutral player neutral] emits a neutral event. *)

    val project : 'a ComputationState.t -> ES.Make(Label).t
    (** Projects a computation state to its event structure. *)

    val view : ES.Make(Label).Event.View.t World.id
  end

  module Monad (S : sig
    val to_string : Label.t -> string
  end)
  (Monad : Monad.T) =
  struct
    module ES = ES.Make (Label)
    module M = ES.MakeMonad (Monad)
    include M

    let game =
      (Location.create_family compare, Location.create_family compare)

    let r = ref 0

    let id () = incr r ; !r

    let rec history pointer address =
      match pointer with
      | Some ptr ->
          address :: history (Label.pointer ptr) (Label.address ptr)
      | None -> [address]

    let play ?pointer player address visible =
      let move : Label.visible =
        {Label.id= id (); player; desc= `Visible {address; visible; pointer}}
      in
      put (Location.at (fst game) (history pointer address)) move
      >> return move

    let wait ?pointer address =
      watch (Location.at (fst game) (history pointer address))

    let neutral player data =
      let move : Label.neutral =
        {Label.id= id (); player; desc= `Neutral data}
      in
      put (Location.at (snd game) player) move >> return move

    let filters : Label.t Domain.Message.Filter.t list =
      Domain.Message.Filter.
        [ (family (fst game) %> fst :> Label.t Domain.Message.Filter.t)
        ; (family (snd game) %> fst :> Label.t Domain.Message.Filter.t) ]

    let project state = get_es state

    let view = view

    module ComputationState = struct
      include M.ComputationState

      let run ?world t u =
        let update w =
          fst
          @@ World.query filters_id
               (function
                 | None -> (Some filters, ())
                 | Some l -> (Some (l @ filters), ()))
               w
        in
        M.ComputationState.run ?world:(Option.map update world) t u
    end
  end
end
