(** A monad for causal and nondeterministic computations. *)

(** We provide a monad that can be used to represent causal and
    nondeterministic computations: a value of type ['a t] represents a
    possibly concurrent computation yielding values of type ['a]. In
    particular, it can return no values or several values.

    The operations provided by the monad are split into three categories:

    1. (Deterministic) Parallelism: Operations to run several threads in
    parallel along with communication primitives. These primitives implement
    deterministic message-passing hence differ from more standard channels à
    la CCS or the π-calculus. To avoid races, messages are never removed
    from the pool: a receive operation sees all messages that have been sent,
    or will be sent on this channel. To avoid confusion with the standard
    concept, we use [put] and [watch] for the name of these operations that
    add a value in the pool, and retrieve all the values from the pool
    respectively.

    2. To represent nondeterminism, we use a concept of *worlds*. Each thread
    is running in a world which encodes the history of the nondeterministic
    choices that have been seen before. Every action is performed within a
    world. To make a nondeterministic choice is simply to update the world
    with a new choice. Two actions are incompatible when they see the same
    choice, but with a different outcome. Communication can only occur
    between compatible worlds.

    3. Running a computation. We provide some primitive to run a computation
    which return all the messages that have been sent during the computation. *)

(** {1 Monad signatures} *)

(** Since there are several ways of implementing these concepts, we provide
    abstract signature which describe exactly the type of these primitives,
    and then describe two implementations: {!Sem} and {!Free}. *)

(** This first signature represents the minimal constructions described
    above. *)
module type Minimal = sig
  (** {1 Monadic interface} *)

  (** Type of computations that can return values of type ['a]. Such
      computations may never return or return several times. *)
  type 'a t

  val return : 'a -> 'a t
  (** [return x] is the computation that only returns [x] *)

  val bind : 'a t -> ('a -> 'b t) -> 'b t
  (** [bind m f] runs [m] and for every value that [m] returns, calls [f] on
      this value, and returns all the results of all the calls to [f] *)

  (** {1 Parallelism} *)
  val parallel : 'a t list -> 'a t
  (** Execute a list of computations in parallel. The return values are all
      all possible return values of computations in argument. For instance
      [parallel \[return 1; return 2\]] returns both 1 and 2 *)

  val discard : 'a t
  (** [discard] is the computation that never returns (and does not perform
      any side effect) *)

  val put_ : 'a Location.t -> 'a -> 'b t -> 'b t
  (** Low-level put: [put_ loc value cont] puts [value] into location [loc]
      and then executes cont. *)

  val watch_ : 'a Location.t -> ('a -> 'b t) -> 'b t
  (** Low-level watch: [put_ loc cont] executes [cont] for every value put in
      [loc]. *)

  (** {1 Nondeterminism through world} *)

  val query_world : (World.t -> World.t * 'a) -> ('a -> 'b t) -> 'b t
  (** [query_world f t] is a computation that executes pure function [f] on
      the current world at the beginning of the computation. This call
      returns a pair [(new_world, result)]. After this call,
      [query_world f t] behaves as the computation [t result] in the world
      [new_world]. *)

  (** {1 Running computations} *)

  (** Alias to avoid shadowing *)
  type 'a m = 'a t

  (** This module defines function to run and interact with computation
      outside the monadic world *)
  module ComputationState : sig
    (** State of a computation being ran. *)
    type 'a t

    val init : ?show:('a -> string) -> unit -> 'a t
    (** Initialises a new computation state. This computation state has no
        results and empty domain. *)

    val run : ?world:World.t -> 'a t -> 'a m -> 'a t
    (** [run state thread] runs [thread] on [state] and returns the resulting
        new state. *)

    val project : 'a t -> 'a list * Domain.t
    (** Given a computation state [t] returns the current list of results
        produced as well as the domain (ie. list of all messages produced) *)

    val results : 'a t -> 'a list
    (** Returns the list of results present in the current state. *)

    val state : 'a t -> Domain.t
    (** Returns the current set of messages of the given state. *)
  end
end

(** This module signature defines more convenient combinators built from the
    ones defined in {!T} *)
module type T = sig
  (** @inline *)
  include Minimal

  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t
  (** Alias for monadic bind *)

  val ( >> ) : 'a t -> 'b t -> 'b t
  (** Monadic bind when the function does not depend on the value of the
      first computation *)

  val ( >|= ) : 'a t -> ('a -> 'b) -> 'b t
  (** Monadic bind when the function is pure. *)

  (** {2 Sending and receiving messages} *)

  val put : 'a Location.t -> 'a -> unit t
  (** [put location value] adds a new message with value [value] at location
      [location], and returns unit. Note that putting value is asynchronous,
      so unit is returned straight away independently of any reception of the
      message. However, it does return something as some implementations
      might perform some side effect (eg. add an event to an event structure)
      before returning unit. *)

  val watch : 'a Location.t -> 'a t
  (** [watch m] is a computation that returns every value put in [m] in the
      past, present, or future. *)

  (** {2 Parallelism} *)
  val join_pair : 'a t -> 'b t -> ('a * 'b) t
  (** [join_pair t u] evaluates [t] and [u] in parallel and for every
      compatible return values [v] of [t] and [v'] of [u], returns [t, u] *)

  val join_list : 'a t list -> 'a list t
  (** Same as {!join}, for a list *)

  val spawn : (unit -> 'a t) -> unit t
  (** [spawn f] starts computation [f ()] in the background and returns right
      way. The result value of [f] is ignored. *)

  (** {2 Nondeterminism} *)
  val sum : 'a t list -> 'a t
  (** [sum ts] makes a nondeterministic choice between the computations in
      [ts] *)

  val pick : 'a list -> 'a t
  (** [pick l] returns an element of [l] picked nondeterministically *)

  val dump_world : unit t
  (** [dump_world] is a computation that outputs on stdout the current world
      and returns unit. Useful for debugging purposes. *)

  val insert :
       ?show:('a -> string)
    -> join:('a -> 'a -> 'a option)
    -> 'a
    -> 'a World.id t
  (** [insert ?show ~join init] inserts a new world in the current state with
      initial value [init], join function [join]. The return value is an id
      that can be used to update the value of this particular world with
      {!query} *)

  val query : 'a World.id -> ('a option -> 'a option * 'b) -> 'b t
  (** [query id f] queries the current value of the world denoted by id [id]
      and executes the function [f] on it which returns a pair
      [(new_value, result)]. The world value is updated to [new_value], and
      the operation returns [result]. In both cases, the option indicates
      presence or absence in the global world: eg. f is called with [None] if
      the component denoted by [id] is not present in the current world, and
      if f returns None, nothing is added to the world. *)

  val get_global_world : World.t t
  (** Return the current (global) world *)

  val set_world : 'a World.id -> 'a -> unit t
  (** [set_world id v] sets the component of the world [id] to value [v]. *)

  val get_world : 'a World.id -> 'a option t
  (** [get_world id] returns the value of component [id] in the current
      world, or None if [id] is not defined. *)

  val get_world_def : 'a World.id -> 'a -> 'a t
  (** [get_world_def id def] returns the value of component [id] or [def] if
      absent. *)

  (** {2 Running computation} *)
  val run : ?show:('a -> string) -> 'a t -> 'a list * Domain.t
  (** Runs a computation and returns the return values, as well as the state
      reached. *)

  val run_debug : ('a -> string) -> 'a t -> unit
  (** [run_debug show t] runs computation [t] and prints the output to stdout
      using [show] to display return values. This is useful for tests. *)

  val results : ?show:('a -> string) -> 'a t -> 'a list
  (** Returns the results of a computation, ran in isolation. *)

  val final_state : ?show:('a -> string) -> 'a t -> Domain.t
  (** Returns the final state of a computation, ran in isolation. The final
      state is the set of messages put during the execution. *)
end

(** {1 Monadic implementations} *)

(** Derive the complete signature from the minimal combinators *)
module Complete (M : Minimal) : T

(** Implementation based on increasing map. In this implementation
    [type 'a t = 'a Mailbox.Adress.t → World.t → IncreasingMap.t]. In
    this case, [bind m f] is computed by creating a new address and executing
    m and f in parallel, [f] reading its value from the channel created while
    [m] outputs to it. This can be quite costly for trivial binds. *)
module IncMap : T

(** Implementation based on continuations. In this implementation
    [type 'a t = World.t -> state -> ('a -> World.t -> state -> state)] and
    values are returned by calling the continuations. This is faster but
    harder to reason on/debug. *)
module Cont : T

(** We provide an implementation based on free monads where we perform some
    optimisations to minimise the number of binds and their scope. ['a t]
    becomes some kind of AST which is only evaluated when [run] is called. To
    actually run the computation, we evaluate the AST on a given other
    implementation. This never calls the bind of semantic, so can be useful
    for implementations with slow binds. *)
module Free (Semantic : Minimal) : T

(** {1 Default interface} *)

(** We now provide a default implementation of the signature {!T}. *)

(** @inline *)
include T
