% Quick Start with Causality

In this article, we see how to install and write your first causal
interpreter using Causality.

# Install causality-monad

You can install the library using opam:

    opam pin https://gitlab.inria.fr/causality/causality-monad.git

Or, you can also install it manually:

    opam install dune ppx_expect
    git clone https://gitlab.inria.fr/causality/causality-monad.git
    cd causality-monad
    dune build && dune install
    
# Sequential interpreter
Let us start with the simple case of a sequential language. The file
for this section is available
[here](https://gitlab.inria.fr/causality/causality-monad/-/blob/master/examples/quickstart.ml).

We start by describing the instructions of our language.

```ocaml
type var = string
(* Arithmetic expressions of our language *)
type expr = Int of int | Var of var | Add of expr * expr
(* The only instruction of our language *)
type instr = Write of var * expr
(* A program is a list of instruction. *)
type program = instr list
```

The next step is to setup the causality library.
```ocaml
open Causality

module ES = ES.Make (struct
  type label = string

  let id = None
end)

open ES.Monad

let ( let* ) = bind
```

The second line instanciates the ES (Event Structure) module with the
type of labels for our events. In this simple example, we simply have
events labelled with strings. Once this instanciation is made, we get
a module `ES` that defines what is an event structure but also a
particular monad `ES.Monad` that we make use of for this interpreter.

We can now start writing the interpreter, with expressions which is non-surprising

```ocaml
let rec eval_expr (env : (var * int) list) = function
  | Int n -> n
  | Var v -> List.assoc v env
  | Add (e, e') -> eval_expr env e + eval_expr env e'
```
Next up is evaluating instructions. We want each write to create an event in the final event structure, for that we make use of the primitive `emit : label -> unit t` (here `label = string`). As a result `eval_instr` becomes monadic:

```ocaml
(* Due to the call to emit, that emits an event, the eval_instr has a monadic
   return type (`(string * int) t`) *)
let rec eval_instr env (Write (var, expr)) =
  let value = eval_expr env expr in
  let* () = emit (Printf.sprintf "W%s:=%d" var value) in
  return ((var, value) :: env)
```

This function modifies the environment by updating the value of the
variable being written. To evaluate a program, we evaluate its
instructions in sequence.

```ocaml
let rec eval_program ?(env = []) = function
  | [] -> return env
  | t :: q ->
      let* env = eval_instr env t in
      eval_program ~env q
```

We can now test our code:

```ocaml
let program =
  [ Write ("x", Int 1)
  ; Write ("y", Int 2)
  ; Write ("z", Int 3)
  ; Write ("w", Add (Var "x", Var "y")) ]

let () = display (fun s -> s) (eval_program program)
```
When run, this code will display the generated DAG:

```sh
ocamlfind ocamlc -linkpkg -package causality quickstart.ml -o quickstart
./quickstart
```
![](examples/quickstart.png)

# Non-sequential model

Having shown the basics, we now move on to a more interesting example
that uses some concurrency primitive
[here](https://gitlab.inria.fr/causality/causality-monad/-/blob/master/examples/quickstart_caus.ml). The
language does not change, but our interpreter will. Instead of
executing instructions in sequence, and end up with a linear order, we
will perform instructions only after their dependencies. At the end,
the DAG we obtain will be the dependency graph of our program.

To do this, we change our environment: from lists of pairs `string *
int`, we move to lists of pairs `string * int t`: instead of having an
immediate integer, we have a computation of type `int`, to be thought
of as a promise. That way, when we evaluate an expression, we only
wait on the promises that the instruction depends on:

```ocaml
let rec eval_expr (env : (var * int t) list) = function
  | Int n -> return n
  | Var v -> List.assoc v env
  | Add (e, e') ->
      let* x = eval_expr env e in
      let* y = eval_expr env e' in
      return (x + y)
```

The case for `Int` returns an immediate promise, while `Var` looks up
the promise in the environment. For addition, we use the monadic bind,
written `let*` to chain promises together.

Finally, we simply have to update `eval_instr`. To represent this
concurrent evaluation, we use Causality's locations. Locations are
similar to channels, however their behaviour is somewhat
different. However, for our use case, they do behave like channels in
CCS.

When evaluating an instruction $x := e$, we will create a location `l`
of type `int`. This location induces a promise `watch l` which waits
for messages on `l`. We can then return right away this promise, and
spawn in the background the calculation of `e` that will eventually
fulfill the promise.

```ocaml
let rec eval_instr env (Write (var, expr)) =
  let l = Location.create () in
  let env' = (var, watch l) :: env in
  let* () =
    spawn (fun () ->
        let* n = eval_expr env expr in
        put l n )
  in
  return env'
```

The rest does not need to be changed since the tye of `eval_instr` is
the same as before. Running the code, we get:

![](examples/quickstart_caus.png)

This dag reflects the dependencies between the instruction of our
program.

Note that this semantics is not quite sound in general, as it does not
preserve two writes on the same variable - which would definitely need
to be included in the dependencies. We leave as an exercise to the
reader on how to fix this issue.

To go beyond and model a more realistic concurrent programming
language, you may head to the [tutorial on shared-memory
programming](SC.html)
