module Cell = Linearisable.Cell (Monad)
module Chan = Channel.Make (Monad)
module Lock = Linearisable.Lock (Monad)
open Monad

(* Test de la monade de base *)
let run = Monad.run

module S = Set.Make (Int)

let print_list f l =
  print_endline @@ String.concat " - " @@ List.map f @@ List.sort compare l

let print_ints l = print_list Int.to_string l

let t m = run_debug Int.to_string m

let t_results m = print_ints @@ results m

let%expect_test _ =
  t (return 1) ;
  [%expect {|
    1
    {1 @ return []} |}]

let%expect_test _ = t discard

let%expect_test _ =
  t (return 1 >|= succ) ;
  [%expect {|
    2
    {2 @ return []} |}]

let%expect_test _ =
  t (parallel [return 1; return 2]) ;
  [%expect {|
    1
    2
    {2 @ return []}
    {1 @ return []} |}]

let%expect_test _ =
  let a = Location.create ~show:Int.to_string ~name:"a" () in
  t (parallel [put a 1 >> discard; watch a]) ;
  [%expect {|
    1
    {1 @ return []}
    {1 @ a []} |}]

let%expect_test _ =
  let a = Location.create ~show:Int.to_string ~name:"a" () in
  t (parallel [put a 1 >> discard; put a 2 >> discard; watch a]) ;
  [%expect
    {|
    1
    2
    {2 @ return []}
    {1 @ return []}
    {2 @ a []}
    {1 @ a []} |}]

let%expect_test _ =
  let a = Location.create ~show:Int.to_string ~name:"a" () in
  t (parallel [put a 1 >> discard; put a 2 >> discard; watch a; watch a]) ;
  [%expect
    {|
    1
    2
    1
    2
    {2 @ return []}
    {1 @ return []}
    {2 @ return []}
    {1 @ return []}
    {2 @ a []}
    {1 @ a []} |}]

let%expect_test _ =
  t
    (let x = Location.create ~show:Int.to_string ~name:"x" () in
     let l = Location.create ~show:Unit.to_string ~name:"l" () in
     parallel
       [ put x 1 >> discard
       ; watch x >> put l () >> discard
       ; watch l >> return 1 ]
     >|= fun n -> n + 1 ) ;
  [%expect {|
    2
    {2 @ return []}
    {() @ l []}
    {1 @ x []} |}]

let%expect_test _ =
  t
    ( join_pair
        (parallel [return 1; return 2])
        (parallel [return 2; return 3])
    >>= fun (x, y) -> return (x + y) ) ;
  [%expect
    {|
    3
    4
    4
    5
    {5 @ return []}
    {4 @ return []}
    {4 @ return []}
    {3 @ return []}
    { @  []}
    { @  []} |}]

let%expect_test _ =
  let l = Location.create ~show:Int.to_string ~name:"a" () in
  t
    ( parallel [put l 5 >> discard; join_list [return 2; return 3; watch l]]
    >|= fun l -> List.fold_left ( * ) 1 l ) ;
  [%expect
    {|
    30
    {30 @ return []}
    { @  []}
    { @  []}
    { @  []}
    {5 @ a []} |}]

let mk_test_cell f =
  let c = Cell.create ~name:"r" ~show:Int.to_string 0 in
  t_results (f c)

let%expect_test _ = mk_test_cell Cell.get ; [%expect {|
    0 |}]

let%expect_test _ =
  mk_test_cell (fun c -> Cell.set c 1 >> Cell.get c) ;
  [%expect {|
    1 |}]

let%expect_test _ =
  mk_test_cell (fun c -> parallel [Cell.set c 1 >> discard; Cell.get c]) ;
  [%expect {|
    0 - 1 |}]

let%expect_test _ =
  mk_test_cell (fun c ->
      parallel [Cell.set c 2 >> discard; Cell.set c 1 >> discard; Cell.get c] ) ;
  [%expect {|
    0 - 1 - 1 - 2 - 2 |}]

let%expect_test _ =
  mk_test_cell (fun c ->
      parallel
        [ Cell.update c (fun n -> (succ n, ())) >> discard
        ; Cell.update c (fun n -> (succ n, ())) >> discard
        ; Cell.get c ] ) ;
  [%expect {|
    0 - 1 - 1 - 2 - 2 |}]

let with_lock lock f =
  Lock.lock lock >> f >>= fun v -> Lock.unlock lock >> return v

let incr c = Cell.get c >>= fun n -> Cell.set c (n + 1)

let%expect_test _ =
  let c = Cell.create 0 and lock = Lock.create () in
  t_results
    ( join_pair (with_lock lock (incr c)) (with_lock lock (incr c))
    >> Cell.get c ) ;
  [%expect {|
    2 - 2 - 2 - 2 - 2 - 2 - 2 - 2 - 2 - 2 |}]

let%expect_test _ =
  let c = Cell.create 0 in
  t_results (join_pair (incr c) (incr c) >> Cell.get c) ;
  [%expect {|
    1 - 1 - 1 - 1 - 2 - 2 |}]

let%expect_test _ =
  let data, flag = Cell.(create 0, create 0) in
  t
    (parallel
       [ Cell.set data 1 >> Cell.set flag 1 >> discard
       ; ( Cell.get flag
         >>= fun n -> if n = 1 then Cell.get data else return (-1) ) ] ) ;
  [%expect
    {|
    -1
    1
    {1 @ return [[[set()@:--->]; [get@:--->]]; [[set()@:--->]; [get@:--->]]]}
    {-1 @ return [[[get@:--->]]]}
    {[get@:--->] @  [[[set()@:--->]; [get@:--->]]; [[set()@:--->]; [get@:--->]]]}
    {[set()@:--->] @  [[[set()@:--->]]]}
    {[get@:--->] @  [[[set()@:--->]; [get@:--->]]; [[set()@:--->]]]}
    {[set()@:--->] @  [[[get@:--->]; [set()@:--->]]; [[set()@:--->]]]}
    {[get@:--->] @  [[[get@:--->]]]}
    {[set()@:--->] @  [[[set()@:--->]]; [[set()@:--->]]]} |}]

(* channels *)
let%expect_test _ =
  let c = Chan.create ~name:"c" ~show:Int.to_string () in
  t (parallel [Chan.send c 1 >> discard; Chan.recv c]) ;
  [%expect {|
    1
    {1 @ return [[2 seen by 1]]}
    {1 @ c []} |}]

let%expect_test _ =
  let c = Chan.create ~name:"c" ~show:Int.to_string () in
  t
    (parallel
       [Chan.send c 1 >> discard; Chan.send c 2 >> discard; Chan.recv c] ) ;
  [%expect
    {|
    1
    2
    {2 @ return [[2 seen by 1]]}
    {1 @ return [[3 seen by 1]]}
    {2 @ c []}
    {1 @ c []} |}]

let%expect_test _ =
  let c = Chan.create ~name:"c" ~show:Int.to_string () in
  t
    (parallel
       [ Chan.send c 1 >> discard
       ; Chan.send c 2 >> discard
       ; Chan.recv c
       ; Chan.recv c ] ) ;
  [%expect
    {|
    1
    2
    1
    2
    {2 @ return [[3 seen by 1]]}
    {1 @ return [[4 seen by 1]]}
    {2 @ return [[3 seen by 2]]}
    {1 @ return [[4 seen by 2]]}
    {2 @ c []}
    {1 @ c []} |}]

let%expect_test _ =
  let c = Chan.create ~name:"c" ~show:Int.to_string () in
  t
    (parallel
       [ Chan.send c 1 >> discard
       ; Chan.send c 2 >> discard
       ; (Chan.recv c >>= fun a -> Chan.recv c >>= fun b -> return (a - b))
       ] ) ;
  [%expect
    {|
    -1
    1
    {1 @ return [[3 seen by 5; 2 seen by 1]]}
    {-1 @ return [[2 seen by 4; 3 seen by 1]]}
    {2 @ c []}
    {1 @ c []} |}]

(* event structures *)

module DebugLocTest = struct
  module ES = ES.Make (struct
    type label = string

    let id = None

    let show x = x
  end)

  module Monad = ES.Monad
  module Cell = Linearisable.Cell (Monad)
  module Channel = Channel.Make (Monad)
  module Lock = Linearisable.Lock (Monad)
  open Monad

  let incr c = Cell.get c >>= fun n -> Cell.set c (n + 1)

  let with_lock lock f =
    Lock.lock lock >> f >>= fun v -> Lock.unlock lock >> return v

  let run_test f =
    print_endline @@ Dot.to_string
    @@ Monad.(dot (fun s -> s) (add_filter Domain.Message.Filter.show >> f))

  let run_test_ f =
    print_endline @@ Dot.to_string @@ Monad.(dot (fun s -> s) f)

  let%expect_test _ =
    let l = Location.create ~name:"l" () in
    run_test Monad.(put l () >> sum [put l (); put l ()]) ;
    [%expect
      {|
      digraph es {
       _1 [id="_1", label="{ @ l []}"]
      _2 [id="_2", label="{ @ l [; ; ]}"]
      _3 [id="_3", label="{ @ l [; ; ]}"]
      _1 -> _2 [arrowhead="empty"]
      _1 -> _3 [arrowhead="empty"]
      _2 -> _3 [color="blue", style="dashed", arrowhead="none"]
       } |}]

  let%expect_test _ =
    let c = Cell.create ~show:string_of_int ~name:"x" 0 in
    run_test (parallel [Cell.set c 1 >> discard; Cell.get c]) ;
    [%expect
      {|
      digraph es {
       _4 [id="_4", label="{[set(1)@x:0--->1] @ x [; [[set(1)@x:0--->1]]]}"]
      _5 [id="_5", label="{[get(=0)@x:0--->0] @ x [; [[get(=0)@x:0--->0]]]}"]
      _6 [id="_6", label="{[set(1)@x:0--->1] @ x [; ; [[get(=0)@x:0--->0]; [set(1)@x:0--->1]]]}"]
      _7 [id="_7", label="{[get(=1)@x:1--->1] @ x [; ; [[set(1)@x:0--->1]; [get(=1)@x:1--->1]]]}"]
      _5 -> _6 [arrowhead="empty"]
      _4 -> _7 [arrowhead="empty"]
      _4 -> _5 [color="blue", style="dashed", arrowhead="none"]
       } |}]

  let%expect_test _ =
    let c = Cell.create ~show:string_of_int ~name:"x" 0 in
    let r = Location.create ~name:"r" ~show:Int.to_string () in
    let lock = Lock.create ~name:"lock" () in
    run_test_
      ( add_filter Domain.Message.Filter.(location r %> Int.to_string)
      >> join_pair (with_lock lock (incr c)) (with_lock lock (incr c))
      >> Cell.get c >>= put r ) ;
    [%expect
      {|
      digraph es {
       _8 [id="_8", label="2"]
      _9 [id="_9", label="2"]
      _10 [id="_10", label="2"]
      _11 [id="_11", label="2"]
      _12 [id="_12", label="2"]
      _13 [id="_13", label="2"]
      _14 [id="_14", label="2"]
      _15 [id="_15", label="2"]
      _16 [id="_16", label="2"]
      _17 [id="_17", label="2"]
      _8 -> _9 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _10 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _11 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _12 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _13 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _14 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _15 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _16 [color="blue", style="dashed", arrowhead="none"]
      _8 -> _17 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _10 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _11 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _12 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _13 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _14 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _15 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _16 [color="blue", style="dashed", arrowhead="none"]
      _9 -> _17 [color="blue", style="dashed", arrowhead="none"]
      _10 -> _11 [color="blue", style="dashed", arrowhead="none"]
      _10 -> _12 [color="blue", style="dashed", arrowhead="none"]
      _10 -> _13 [color="blue", style="dashed", arrowhead="none"]
      _10 -> _14 [color="blue", style="dashed", arrowhead="none"]
      _10 -> _15 [color="blue", style="dashed", arrowhead="none"]
      _10 -> _16 [color="blue", style="dashed", arrowhead="none"]
      _11 -> _12 [color="blue", style="dashed", arrowhead="none"]
      _11 -> _13 [color="blue", style="dashed", arrowhead="none"]
      _11 -> _14 [color="blue", style="dashed", arrowhead="none"]
      _11 -> _15 [color="blue", style="dashed", arrowhead="none"]
      _11 -> _17 [color="blue", style="dashed", arrowhead="none"]
      _12 -> _13 [color="blue", style="dashed", arrowhead="none"]
      _12 -> _14 [color="blue", style="dashed", arrowhead="none"]
      _12 -> _16 [color="blue", style="dashed", arrowhead="none"]
      _12 -> _17 [color="blue", style="dashed", arrowhead="none"]
      _13 -> _15 [color="blue", style="dashed", arrowhead="none"]
      _13 -> _16 [color="blue", style="dashed", arrowhead="none"]
      _13 -> _17 [color="blue", style="dashed", arrowhead="none"]
      _14 -> _15 [color="blue", style="dashed", arrowhead="none"]
      _14 -> _16 [color="blue", style="dashed", arrowhead="none"]
      _14 -> _17 [color="blue", style="dashed", arrowhead="none"]
      _15 -> _16 [color="blue", style="dashed", arrowhead="none"]
      _15 -> _17 [color="blue", style="dashed", arrowhead="none"]
      _16 -> _17 [color="blue", style="dashed", arrowhead="none"]
       } |}]

  let%expect_test _ =
    let flag = Cell.create ~show:Int.to_string ~name:"flag" 0
    and data = Cell.create ~show:Int.to_string ~name:"data" 0 in
    run_test
      Monad.(
        parallel
          [ Cell.set data 1 >> Cell.set flag 1 >> discard
          ; ( Cell.get flag
            >>= fun flag -> if flag = 1 then Cell.get data else return (-1)
            ) ]) ;
    [%expect
      {|
      digraph es {
       _18 [id="_18", label="{[set(1)@data:0--->1] @ data [; [[set(1)@data:0--->1]]]}"]
      _19 [id="_19", label="{[set(1)@flag:0--->1] @ flag [; ; [[set(1)@flag:0--->1]]; [[set(1)@data:0--->1]]]}"]
      _20 [id="_20", label="{[get(=0)@flag:0--->0] @ flag [; [[get(=0)@flag:0--->0]]]}"]
      _21 [id="_21", label="{[set(1)@flag:0--->1] @ flag [; ; [[get(=0)@flag:0--->0]; [set(1)@flag:0--->1]]; [[set(1)@data:0--->1]]]}"]
      _22 [id="_22", label="{[get(=1)@flag:1--->1] @ flag [; ; [[set(1)@flag:0--->1]; [get(=1)@flag:1--->1]]; [[set(1)@data:0--->1]]]}"]
      _23 [id="_23", label="{[get(=1)@data:1--->1] @ data [; ; [[set(1)@flag:0--->1]; [get(=1)@flag:1--->1]]; [[set(1)@data:0--->1]; [get(=1)@data:1--->1]]]}"]
      _18 -> _19 [arrowhead="empty"]
      _18 -> _21 [arrowhead="empty"]
      _20 -> _21 [arrowhead="empty"]
      _19 -> _22 [arrowhead="empty"]
      _22 -> _23 [arrowhead="empty"]
      _19 -> _20 [color="blue", style="dashed", arrowhead="none"]
       } |}]

  let%expect_test _ =
    let c = Channel.create ~name:"c" ~show:Int.to_string () in
    let r = Location.create ~show:Int.to_string ~name:"r" () in
    run_test
      (parallel
         [ Channel.send c 1 >> discard
         ; Channel.send c 2 >> discard
         ; ( Channel.recv c
           >>= fun a -> Channel.recv c >>= fun b -> put r (a - b) ) ] ) ;
    [%expect
      {|
      digraph es {
       _24 [id="_24", label="{1 @ c []}"]
      _25 [id="_25", label="{2 @ c []}"]
      _26 [id="_26", label="{-1 @ r [; ; [2 seen by 4; 3 seen by 1]]}"]
      _27 [id="_27", label="{1 @ r [; ; [3 seen by 5; 2 seen by 1]]}"]
      _24 -> _26 [arrowhead="empty"]
      _25 -> _26 [arrowhead="empty"]
      _24 -> _27 [arrowhead="empty"]
      _25 -> _27 [arrowhead="empty"]
      _26 -> _27 [color="blue", style="dashed", arrowhead="none"]
       } |}]

  let%expect_test _ =
    let c = Channel.create ~name:"c" ~show:Int.to_string () in
    let r = Location.create ~show:Int.to_string ~name:"r" () in
    run_test
      (parallel
         [ Channel.send c 1 >> discard
         ; Channel.recv c >>= put r
         ; Channel.recv c >>= put r ] ) ;
    [%expect
      {|
      digraph es {
       _28 [id="_28", label="{1 @ c []}"]
      _29 [id="_29", label="{1 @ r [; ; [3 seen by 2]]}"]
      _30 [id="_30", label="{1 @ r [; ; [3 seen by 1]]}"]
      _28 -> _29 [arrowhead="empty"]
      _28 -> _30 [arrowhead="empty"]
      _29 -> _30 [color="blue", style="dashed", arrowhead="none"]
       } |}]

  let%expect_test _ =
    let c = Channel.create ~name:"c" ~show:Int.to_string () in
    let r = Location.create ~show:Int.to_string ~name:"r" () in
    run_test
      (parallel
         [ Channel.send c 1 >> discard
         ; Channel.send c 2 >> discard
         ; Channel.recv c >>= put r ] ) ;
    [%expect
      {|
      digraph es {
       _31 [id="_31", label="{1 @ c []}"]
      _32 [id="_32", label="{2 @ c []}"]
      _33 [id="_33", label="{1 @ r [; ; [3 seen by 1]]}"]
      _34 [id="_34", label="{2 @ r [; ; [2 seen by 1]]}"]
      _31 -> _33 [arrowhead="empty"]
      _32 -> _34 [arrowhead="empty"]
      _33 -> _34 [color="blue", style="dashed", arrowhead="none"]
       } |}]

  let%expect_test _ =
    let c = Channel.create ~name:"c" ~show:Int.to_string () in
    let r = Location.create ~show:Int.to_string ~name:"r" () in
    run_test
      (parallel
         [ Channel.send c 1 >> Channel.send c 2 >> discard
         ; ( Channel.recv c
           >>= fun x -> Channel.recv c >>= fun y -> put r (x - y) ) ] ) ;
    [%expect
      {|
      digraph es {
       _35 [id="_35", label="{1 @ c []}"]
      _36 [id="_36", label="{2 @ c [; ]}"]
      _37 [id="_37", label="{-1 @ r [; ; [2 seen by 4; 3 seen by 1]]}"]
      _38 [id="_38", label="{1 @ r [; ; [3 seen by 5; 2 seen by 1]]}"]
      _35 -> _36 [arrowhead="empty"]
      _36 -> _37 [arrowhead="empty"]
      _36 -> _38 [arrowhead="empty"]
      _37 -> _38 [color="blue", style="dashed", arrowhead="none"]
       } |}]
end

let%expect_test _ =
  let flag = Cell.create ~name:"flag" 0
  and data = Cell.create ~name:"data" 0 in
  print_list
    (function None -> "none" | Some n -> Printf.sprintf "some(%d)" n)
    (results
       Monad.(
         parallel
           [ Cell.set data 17 >> Cell.set flag 1 >> discard
           ; ( Cell.get flag
             >>= fun n ->
             if n = 0 then return None else Cell.get data >|= fun x -> Some x
             ) ]) ) ;
  [%expect {| none - some(17) |}]
