(** Locations - places where to store messages. *)

(** We define here the type of locations, which can be thought as mere
    addresses (eg. integer). Allocation of new locations increment a counter.

    However, we also need the possibility to allocate infinitely many
    locations at once, this is used for the encoding of cells and channels. *)

(** {1 Locations} *)

(** The type of mailboxes that can store values of type ['a]. One can think
    of them as being mere integers. *)
type 'a t

val create : ?show:('a -> string) -> ?name:string -> unit -> 'a t
(** Creates a new mailbox. The parameter [name] and [show] can be used for
    debugging purposes. *)

val show : 'a t -> string
(** Give a string representation of the mailbox. *)

val show_payload : 'a t -> 'a -> string
(** Give a string representation of a payload for a location *)

val compare : 'a t -> 'b t -> ('a, 'b) PolyMap.compare
(** Compare two addresses. *)

(** {1 Location family} *)

(** In some use cases, one might need to allocate an infinte amount of
    locations that can be addressed by a certain type.

    For that, one can use a location family. *)

(** The type of location families. ['address] is the type of index of the
    family, while ['value] is the type of values carried by locations of this
    family. *)
type ('value, 'address) family

val at : ('value, 'address) family -> 'address -> 'value t
(** Given a family and an address, get the location at that specific address. *)

val create_family :
     ?show_value:('value -> string)
  -> ?show_address:('address -> string)
  -> ?name:string
  -> ('address -> 'address -> int)
  -> ('value, 'address) family
(** This creates a new family of mailboxes that are indexed over the type
    ['address]. *)

val cast_family :
  ('value, 'address) family -> 'b t -> ('address * ('b -> 'value)) option
(** [cast_family family loc] checks whether [loc] belongs to [family]. If
    not, it returns [None]. Otherwise, it returns the address of [loc] in
    [family] as well as a function from the type of values of loc to those of
    [family] (since those are the same type. *)

val cast : 'a t -> 'b t -> ('b -> 'a) option
(** [cast l1 l2] checks whether l1 and l2 are the same location, and if so
    return a cast function for values *)
