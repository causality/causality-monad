(** Dot: a thin layer of abstraction over the Dot format. *)

module Attribute : sig
  type t = {name: string; value: string}

  val arrowhead : string -> t

  val color : string -> t

  val style : string -> t

  val fillcolor : string -> t

  val shape : string -> t

  val class_ : string -> t

  val id : string -> t

  val label : string -> t
end

module Node : sig
  type t

  val make : name:string -> attrs:Attribute.t list -> t

  val name : t -> string
end

module Edge : sig
  type t

  val make : src:string -> tgt:string -> attrs:Attribute.t list -> t
end

(** The type of dot graphs: a list of nodes and edges *)
type t = {nodes: Node.t list; edges: Edge.t list}

val to_string : ?name:string -> t -> string
(** Converts an abstraction representation to a Dot-formatted string. *)

val to_file : t -> string -> unit
(** [to_file graph fname] outputs the representation of [graph] to [fname]. *)

val to_pdf : t -> string -> unit
(** [to_pdf graph fname] outputs the representation of [graph] in PDF.
    Requires Dot. *)

val to_png : t -> string -> unit
(** [to_png graph fname] outputs the representation of [graph] in PNG.
    Requires Dot. *)

val view : ?viewer:string -> t -> unit
(** View a graph by calling a viewer. This needs the binary dot. *)

(** {1 Attributes} *)
