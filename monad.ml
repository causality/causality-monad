module type Minimal = sig
  type 'a t

  val return : 'a -> 'a t

  val bind : 'a t -> ('a -> 'b t) -> 'b t

  val parallel : 'a t list -> 'a t

  val discard : 'a t

  val put_ : 'a Location.t -> 'a -> 'b t -> 'b t

  val watch_ : 'a Location.t -> ('a -> 'b t) -> 'b t

  val query_world : (World.t -> World.t * 'a) -> ('a -> 'b t) -> 'b t

  type 'a m = 'a t

  module ComputationState : sig
    type 'a t

    val init : ?show:('a -> string) -> unit -> 'a t

    val run : ?world:World.t -> 'a t -> 'a m -> 'a t

    val project : 'a t -> 'a list * Domain.t

    val results : 'a t -> 'a list

    val state : 'a t -> Domain.t
  end
end

module type T = sig
  include Minimal

  val ( >>= ) : 'a t -> ('a -> 'b t) -> 'b t

  val ( >> ) : 'a t -> 'b t -> 'b t

  val ( >|= ) : 'a t -> ('a -> 'b) -> 'b t

  val put : 'a Location.t -> 'a -> unit t

  val watch : 'a Location.t -> 'a t

  val join_pair : 'a t -> 'b t -> ('a * 'b) t

  val join_list : 'a t list -> 'a list t

  val spawn : (unit -> 'a t) -> unit t

  val sum : 'a t list -> 'a t

  val pick : 'a list -> 'a t

  val dump_world : unit t

  val insert :
       ?show:('a -> string)
    -> join:('a -> 'a -> 'a option)
    -> 'a
    -> 'a World.id t

  val query : 'a World.id -> ('a option -> 'a option * 'b) -> 'b t

  val get_global_world : World.t t

  val set_world : 'a World.id -> 'a -> unit t

  val get_world : 'a World.id -> 'a option t

  val get_world_def : 'a World.id -> 'a -> 'a t

  val run : ?show:('a -> string) -> 'a t -> 'a list * Domain.t

  val run_debug : ('a -> string) -> 'a t -> unit

  val results : ?show:('a -> string) -> 'a t -> 'a list

  val final_state : ?show:('a -> string) -> 'a t -> Domain.t
end

module Complete (M : Minimal) : T with type 'a t = 'a M.t = struct
  include M

  let ( >>= ) = bind

  let ( >> ) m m' = bind m (fun _ -> m')

  let ( >|= ) m f = bind m (fun x -> return (f x))

  let put loc msg = put_ loc msg (return ())

  let watch loc = watch_ loc return

  let insert ?show ~join init =
    query_world (World.insert ?show ~join init) return

  let query id f = query_world (World.query id f) return

  let spawn f = parallel [f () >> discard; return ()]

  let sum ts =
    let join w w' = if w = w' then Some w else None in
    let id = World.create_id ~join () in
    parallel
      (List.mapi
         (fun i thread -> query id (fun _ -> (Some i, ())) >> thread)
         ts)

  let pick l = sum (List.map return l)

  let join_pair t u =
    let a = Location.create () in
    parallel
      [ t >>= put a >> discard
      ; (u >>= fun y -> watch a >>= fun x -> return (x, y)) ]

  let rec join_list = function
    | [] -> return []
    | t :: q -> join_pair t (join_list q) >|= fun (x, xs) -> x :: xs

  let get_global_world = query_world (fun x -> (x, x)) return

  let set_world id v = query id (fun _ -> (Some v, ()))

  let get_world id = query id (fun x -> (x, x))

  let dump_world =
    get_global_world
    >>= fun w ->
    print_endline (World.show w) ;
    return ()

  let get_world_def id default =
    query id (fun x -> (x, Option.value ~default x))

  let run ?show x = ComputationState.(project (run (init ?show ()) x))

  let results ?show x = fst (run ?show x)

  let final_state ?show x = snd (run ?show x)

  let run_debug show computation =
    let results, domain = run ~show computation in
    List.iter (fun value -> print_endline (show value)) results ;
    print_endline @@ Domain.dump domain
end

let messages state ~at =
  let filter msg =
    PolyMap.cast (Location.compare msg.Domain.Message.at at) msg.payload
  in
  List.filter_map filter (Domain._match state at)

module IncMapMinimal = struct
  type 'a t = 'a Location.t -> World.t -> IncreasingMap.t

  let return payload at world =
    IncreasingMap.emit Domain.Message.{at; world; payload}

  let put_ at payload cont address world =
    IncreasingMap.(
      emit Domain.Message.{at; payload; world} +++ cont address world)

  let watch_ loc k addr world =
    IncreasingMap.watch loc (fun msg ->
        match World.join world msg.Domain.Message.world with
        | Some world' -> Some (k msg.Domain.Message.payload addr world')
        | None -> None)

  let query_world f k addr world =
    let world, result = f world in
    k result addr world

  let bind m f address world =
    let temp = Location.create () in
    IncreasingMap.(m temp world +++ watch_ temp f address world)

  let parallel ms address world =
    IncreasingMap.union (List.map (fun f -> f address world) ms)

  let discard _address _world = IncreasingMap.empty

  type 'a m = 'a t

  module ComputationState = struct
    type 'a t = {ret_addr: 'a Location.t; map: IncreasingMap.t}

    let init ?show () =
      let ret_addr = Location.create ?show ~name:"return" () in
      {ret_addr; map= IncreasingMap.empty}

    let project {ret_addr; map} =
      let d = IncreasingMap.init map in
      (messages d ~at:ret_addr, d)

    let results {ret_addr; map} =
      let d = IncreasingMap.init map in
      messages d ~at:ret_addr

    let state {map; _} =
      let d = IncreasingMap.init map in
      d

    let run ?(world = World.empty) state thread =
      let open IncreasingMap in
      let map = add_and_close state.map (thread state.ret_addr world) in
      {state with map}
  end
end

module ContMinimal = struct
  type ('a, 'st) cont = 'a -> World.t -> 'st -> 'st

  module ValueHandler = struct
    type ('a, 'r) t = World.t * ('a, 'r) cont
  end

  module Handlers = PolyMap.MakeRel (Location) (ValueHandler)

  type state = S of Domain.t * state Handlers.t

  and 'a t = World.t -> state -> ('a, state) cont -> state

  let return x world state k = k x world state

  let bind m f world state k =
    m world state (fun x world state -> f x world state k)

  let par t u world state k =
    let state' = t world state k in
    u world state' k

  let discard _ state _ = state

  let parallel = function [] -> discard | t :: q -> List.fold_left par t q

  let put_ location value cont world (S (msgs, handlers)) k =
    let state =
      S
        ( Domain.add Domain.Message.{at= location; payload= value; world} msgs
        , handlers )
    in
    let handlers = Handlers.lookup location handlers in
    let fold state (world', handler) =
      match World.join world world' with
      | None -> state
      | Some w -> handler value w state
    in
    let state = List.fold_left fold state handlers in
    cont world state k

  let watch_ location cont world (S (msgs, handlers)) k =
    let state =
      S
        ( msgs
        , Handlers.add location
            (world, fun value world state -> cont value world state k)
            handlers )
    in
    let msgs = Domain._match msgs location in
    let fold state msgs =
      match World.join world msgs.Domain.Message.world with
      | None -> state
      | Some w -> cont msgs.Domain.Message.payload w state k
    in
    List.fold_left fold state msgs

  let query_world qf cont world state k =
    let world', res = qf world in
    cont res world' state k

  type 'a m = 'a t

  module ComputationState = struct
    type 'a t = 'a list ref * state

    let init ?show:_ () = (ref [], S (Domain.bottom, Handlers.empty))

    let project (r, S (msgs, _)) = (!r, msgs)

    let results (r, _) = !r

    let state (_, S (msgs, _)) = msgs

    let run ?(world = World.empty) (r, state) comp =
      ( r
      , comp world state (fun x _ state ->
            r := x :: !r ;
            state) )
  end
end

module IncMap = Complete (IncMapMinimal)
module Cont = Complete (ContMinimal)

module FreeMinimal (Semantic : Minimal) = struct
  type 'a t =
    | Pure : 'a -> 'a t
    | Put : 'a Location.t * 'a -> 'b t
    | Watch : 'a Location.t * ('a -> 'b t) -> 'b t
    | Parallel : 'a t list -> 'a t
    | Query : (World.t -> World.t * 'a) * ('a -> 'b t) -> 'b t

  let return x = Pure x

  let i = ref 0

  let emit at value = Put (at, value)

  let discard = Parallel []

  let rec sem m =
    match m with
    | Pure x -> Semantic.return x
    | Put (mbox, v) -> Semantic.put_ mbox v Semantic.discard
    | Watch (mbox, f) -> Semantic.watch_ mbox (fun v -> sem (f v))
    | Parallel l -> Semantic.parallel (List.map (fun f -> sem f) l)
    | Query (f, g) -> Semantic.query_world f (fun v -> sem (g v))

  let parallel ms = Parallel ms

  let query_world f g = Query (f, g)

  let put_ a b cont = Parallel [Put (a, b); cont]

  let watch_ a cont = Watch (a, cont)

  let rec bind m f =
    match m with
    | Pure x -> f x
    | Put (a, b) -> Put (a, b)
    | Watch (mbox, cont) -> Watch (mbox, fun x -> bind (cont x) f)
    | Parallel l -> Parallel (List.map (fun x -> bind x f) l)
    | Query (g, phi) -> Query (g, fun x -> bind (phi x) f)

  type 'a m = 'a t

  module ComputationState = struct
    include Semantic.ComputationState

    let run ?world x y = Semantic.ComputationState.run ?world x (sem y)
  end
end

module Free (Sem : Minimal) = Complete (FreeMinimal (Sem))
include Free (IncMap)
