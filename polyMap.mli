(** Polymorphic maps. *)

(** This module allows us to build a map whose type of keys is a parametrised
    [α Key.t]. It behaves similarly as a map from the standard module {!Map}
    where the type of keys is [∃α. α Key.t] except that it guarantees
    that a key of type α Key.t is bound to a value α Value.t: the type of
    value can depend on the type of keys. We also add a second type to
    values, which is constant throughout a given map, and used when building
    recursive types out of polymaps. *)

(** First, before defining the functor, we have to define the type of
    comparison. When all keys have the same type, an int is enough to store
    the result of the comparison, however, when having parametrised keys, we
    want a polymorphic comparison [compare: 'a key -> 'b key -> ..], which
    further more only returns equality when ['a] and ['b] are actually equal.
    For that purpose we define a parametrised comparison type, which is GADT,
    and allows equality when the parameter types are equal. *)

type ('a, 'b) compare =
  | Less : ('a, 'b) compare
  | Equal : ('a, 'a) compare
  | Greater : ('a, 'b) compare

module type Ord = sig
  type 'a t

  val compare : 'a t -> 'b t -> ('a, 'b) compare
end

val cast : ('a, 'b) compare -> 'a -> 'b option
(** [cast cmp_result x] returns None if [cmp_result] is not equal. If it is
    equal, then it returns [Some x], with a different type. *)

(** We first start with functional maps, that map a key to a single value *)
module MakeFunc
    (Key : Ord) (Value : sig
      type ('a, 'r) t
    end) : sig
  type 'r t

  val empty : 'r t
  (** The empty map *)

  type 'r merge_fun =
    { merge:
        'a.    'a Key.t -> ('a, 'r) Value.t -> ('a, 'r) Value.t
        -> ('a, 'r) Value.t option }

  val merge : 'r merge_fun -> 'r t -> 'r t -> 'r t option

  val add : 'a Key.t -> ('a, 'r) Value.t -> 'r t -> 'r t
  (** Add a binding to a map *)

  val update :
       'a Key.t
    -> (('a, 'r) Value.t option -> ('a, 'r) Value.t option * 'c)
    -> 'r t
    -> 'r t * 'c

  val singleton : 'a Key.t -> ('a, 'r) Value.t -> 'r t
  (** Map with a single binding *)

  val lookup : 'a Key.t -> 'r t -> ('a, 'r) Value.t
  (** Lookup all the bindings of a given mailbox *)

  (** The type of bindings for such maps - an existential type *)
  type 'r binding = B : 'a Key.t * ('a, 'r) Value.t -> 'r binding

  val bindings : 'r t -> 'r binding list
  (** Returns the binding of a map *)

  val fold : ('a -> 'r binding -> 'a) -> 'a -> 'r t -> 'a

  val is_empty : 'r t -> bool
end

(** From {!MakeFunc}, we derive [MakeRel] where a key is mapped to a list of
    values. *)
module MakeRel (Key : sig
  type 'a t

  val compare : 'a t -> 'b t -> ('a, 'b) compare
end) (Value : sig
  type ('a, 'r) t
end) : sig
  (** A map mapping ['a Key.t] to a list of [('a, 'r) Value.t]. The ['r] is a
      parameter that is useful when defining recursive types involving
      polymaps, eg. [type t = t PolyMap.t] *)
  type 'r t

  val empty : 'r t
  (** The empty map *)

  val ( +++ ) : 'r t -> 'r t -> 'r t
  (** Union of two maps. If a mailbox is defined on both side, we concatenate
      both the lists. *)

  val union : 'r t list -> 'r t
  (** Union of a list of maps. If a mailbox is defined on several side, we
      concatenate the lists. *)

  val add : 'a Key.t -> ('a, 'r) Value.t -> 'r t -> 'r t
  (** Add a binding to a map *)

  val singleton : 'a Key.t -> ('a, 'r) Value.t -> 'r t
  (** Map with a single binding *)

  val lookup : 'a Key.t -> 'r t -> ('a, 'r) Value.t list
  (** Lookup all the bindings of a given mailbox *)

  (** {2 Extracting bindings} *)

  (** The type of bindings for such maps - an existential type *)

  type 'r binding = B : 'a Key.t * ('a, 'r) Value.t -> 'r binding

  val bindings : 'r t -> 'r binding list
  (** Returns the binding of a map *)

  val fold : ('a -> 'r binding -> 'a) -> 'a -> 'r t -> 'a

  val is_empty : 'r t -> bool
end
