% The Causality Monad: A Monad for Concurrency

`Causality` is a OCaml library that provides a monad for concurrent
programming. Its aim is to allow writing of causal semantics of
programming languages in a modular and high-level way, while still
having an operational flavour (ie. describe how the program is
run). It supports general concurrent and nondeterministic
computations. Several implementations of the monadic operations exist
that all run the concurrent program sequentially and
deterministically, and return *all* possible behaviours. For racy
programs, it returns the final states according to all possible races
without having to compute all interleavings. More than computing final
states, computations can be run to obtain an [event
structure](https://en.wikipedia.org/wiki/Event_structure), a graph
containing all possible actions of the program (events) and their
causal relationship.

- [Source Code](https://gitlab.inria.fr/causality/causality-monad)
- [Documentation of the library](https://iso.mor.phis.me/software/causality/causality-monad/doc/causality/Causality/index.html)
- [Manual](MANUAL.html)
- [Quick start](quick-start.html)

# Installing
The API of the library is still subject to change, so it is not
officially on the opam repository yet. There are two ways to install the library:

- From the source
  ```sh
  git clone https://gitlab.inria.fr/causality/causality-monad.git
  cd causality-monad
  dune build && dune install
  ```
- From a temporary opam package
  ```sh
  opam pin https://gitlab.inria.fr/causality/causality-monad.git

# Implementation details {#sec:implem}
We now go over some implementation details, on how to bridge the gap
from the ideas described in [@sec:math], to the actual code. We have
written a few custom layers over standard OCaml data structures to
overcome their typing which is too rigid. For instance, to formalise
the domain of messages, we need to have an existential type: `(∃α. (α
Location.t * α * World.t)) list`.

## Custom data structure
### [PolyMap](https://iso.mor.phis.me/software/causality/causality-monad/doc/causality/Causality/PolyMap/)
PolyMap is a layer over the OCaml module Map wich provides a
representation of relations (ie. sets of pairs) convienent for lookups
of element by their key (first projection). However, the type of key
and values must be uniform. However, to represent a set of messages,
we want to represent it as a relation `(location, message)` to allow
fast retrieval of the set of messages at a particular location. This
means that we want parametrised types `α key`, `α value` and to be
able to store pairs `(α key, α value)` with maintaining the invariant
that for a given pair, the `α` is indeed the same. The PolyMap module
does this.

## Smart representation of increasing maps
Another aspect that makes the implementation somewhat nontrivial is
our representation of increasing maps on the domain of messages (for
the implementation of the monad) based on increasing maps. Instead of
functions from domains to domains, we use a more efficient
representation which avoids some redundant computation, especially
when computing the fixpoint. Since all such maps can do is add new
messages, and react to messages present in the current state, we
represent them as pairs of a multiset of messages, and a multiset of
handlers.
