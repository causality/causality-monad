type ('a, 'b) compare =
  | Less : ('a, 'b) compare
  | Equal : ('a, 'a) compare
  | Greater : ('a, 'b) compare

module type Ord = sig
  type 'a t

  val compare : 'a t -> 'b t -> ('a, 'b) compare
end

let cast (type a b) (x : (a, b) compare) (y : a) : b option =
  match x with Equal -> Some y | _ -> None

module MakeFunc
    (Key : Ord) (Value : sig
      type ('a, 'r) t
    end) =
struct
  type key = HideKey : 'a Key.t -> key

  type 'r value = HideValue : ('a, 'r) Value.t -> 'r value

  module M = Map.Make (struct
    type t = key

    let compare (HideKey x) (HideKey y) =
      match Key.compare x y with Equal -> 0 | Greater -> 1 | Less -> -1
  end)

  type 'r t = 'r value M.t

  let empty = M.empty

  let add key value m = M.add (HideKey key) (HideValue value) m

  let singleton key value = add key value empty

  type 'r merge_fun =
    { merge:
        'a.    'a Key.t -> ('a, 'r) Value.t -> ('a, 'r) Value.t
        -> ('a, 'r) Value.t option }

  let decrypt (type a) (_ : a Key.t) (HideValue v) : (a, 'r) Value.t =
    Obj.magic v

  let merge {merge} r r' =
    let exception Failed in
    try
      let result =
        M.union
          (fun (HideKey key) v v' ->
            match merge key (decrypt key v) (decrypt key v') with
            | Some v -> Some (HideValue v)
            | None -> raise Failed)
          r r'
      in
      Some result
    with Failed -> None

  let lookup key map = decrypt key (M.find (HideKey key) map)

  type 'r binding = B : 'a Key.t * ('a, 'r) Value.t -> 'r binding

  let bindings m =
    List.map (fun (HideKey c, v) -> B (c, decrypt c v)) (M.bindings m)

  let fold f acc map = List.fold_left f acc (bindings map)

  let update key f m =
    let wrap = Option.map (fun x -> HideValue x) in
    let r = ref None in
    let invoke x =
      let y, res = f x in
      r := Some res ;
      wrap y
    in
    let map =
      M.update (HideKey key)
        (function
          | None -> invoke None | Some v -> invoke (Some (decrypt key v)))
        m
    in
    match !r with
    | Some x -> (map, x)
    | None -> (
        let y, res = f None in
        match y with None -> (map, res) | Some y -> (add key y map, res) )

  let is_empty = M.is_empty
end

module MakeRel
    (Key : Ord) (Value : sig
      type ('a, 'r) t
    end) =
struct
  module M =
    MakeFunc
      (Key)
      (struct
        type ('a, 'r) t = ('a, 'r) Value.t list
      end)

  type 'r t = 'r M.t

  let empty = M.empty

  let ( +++ ) m m' =
    match M.merge {merge= (fun _ l l' -> Some (l @ l'))} m m' with
    | None -> assert false
    | Some l -> l

  let union = function [] -> M.empty | t :: q -> List.fold_left ( +++ ) t q

  let add c t m =
    fst
    @@ M.update c
         (function Some l -> (Some (t :: l), ()) | None -> (Some [t], ()))
         m

  let singleton c t = add c t empty

  let lookup k map = try M.lookup k map with Not_found -> []

  type 'r binding = B : 'a Key.t * ('a, 'r) Value.t -> 'r binding

  let bindings map =
    List.concat
    @@ List.map
         (fun (M.B (x, l)) -> List.map (fun y -> B (x, y)) l)
         (M.bindings map)

  let fold f acc map = List.fold_left f acc (bindings map)

  let is_empty = M.is_empty
end
