(** Representing linearisable datatype.

    We provide here an implementation of a linearisable concurrent datatype.
    Queries and updates can be performed on the datatype, with the guarantee
    that all the operations are executed in a linear order. *)

(** The type of concurrent datatype whose state is ['a] *)
type 'a t

module Transaction : sig
  type 'state t =
    | E :
        { name: string
        ; operation: string
        ; input: 'state
        ; output: 'state
        ; number: int
        ; show_result: 'result -> string
        ; id: int
        ; result: 'result }
        -> 'state t

  val show : ('a -> string) -> 'a t -> string
end

module Trace : sig
  type 'state t

  val show : ('a -> string) -> 'a t -> string
end

(** Implementation of a concurrent datatype on top of an implementation of
    {!Monad.T} *)

module Make (Monad : Monad.T) : sig
  val create : ?show:('a -> string) -> ?name:string -> 'a -> 'a t
  (** Creates a new instance of a datatype with the given initial value. The
      optional parameter [show] and [name] are useful for debugging purpose. *)

  val perform :
       ?name:string
    -> ?show_result:('b -> string)
    -> ?act:('b -> unit Monad.t)
    -> ?conflict:('a Transaction.t -> bool)
    -> 'a t
    -> ('a -> ('a * 'b) Monad.t)
    -> 'b Monad.t
  (** [perform instance f] calls [f] on the current value of [instance] and
      updates the new value of [instance] with the first component of the
      return value of [f] and returns the second. While [f] is allowed to be
      effectful, it is unclear what happens when too fancy effects are
      applied in [f].

      Concurrent calls to perform will be sequentialised in an arbitrary way,
      thus leading to a race.

      The optional parameters [name] and [show_result] are useful for
      debugging purpose. (name being the name of the operation performed, eg.
      "get" or "set").

      The optional parameter [act] denotes an action to be performed just
      after the operation has been performed locally, and just before that
      transaction is broadcasted.

      The optional parameter [conflict] says whether a given transaction is
      meant to conflict with the transaction being performed. When two
      transactions conflict, it means they have to be ordered. *)

  val filter : 'a t -> 'a Transaction.t Domain.Message.Filter.t
end

(** Concurrent memory cell *)
module Cell (Monad : Monad.T) : sig
  (** Type of concurrent memory cell with values of type ['a] *)
  type 'a t

  val create : ?show:('a -> string) -> ?name:string -> 'a -> 'a t
  (** Create a new cell *)

  val get : ?act:('a -> unit Monad.t) -> 'a t -> 'a Monad.t
  (** Get the contents of a cell *)

  val set : ?act:(unit -> unit Monad.t) -> 'a t -> 'a -> unit Monad.t
  (** Set the contents of a cell *)

  val update :
       ?name:string
    -> ?show:('b -> string)
    -> ?act:('b -> unit Monad.t)
    -> 'a t
    -> ('a -> 'a * 'b)
    -> 'b Monad.t
  (** Perform an atomic update. [update ?name ?show cell f] computes f on the
      current value of the cell, updates the value of the cell with the first
      component of the result of [f] and returns the second part. [?name] and
      [?show] are parameters for debugging purposes. *)

  val filter : 'a t -> 'a Transaction.t Domain.Message.Filter.t
end

module Lock (Monad : Monad.T) : sig
  (** Type of locks *)
  type t

  val create : ?name:string -> unit -> t
  (** Create a new lock. *)

  val lock : t -> unit Monad.t
  (** Acquire the lock. *)

  val unlock : t -> unit Monad.t
  (** Release the lock. *)
end
