(** Game Semantics *)

(** This module implements basic game semantics based on event structures on
    top of causality. This simple model is perfect for untyped models (or
    typed models viewed as untype) playing on a universal arena. *)

(** A game semantics is produced out of signature, describing visible moves
    as well as internal events. *)

module type Signature = sig
  (** An address is a description of an initial cell of the universal arena.
      We assume that the arena is recursive and that for any minimal e, G/e
      ≅ G, ie. the type of address remains valid deep inside G. *)
  type address

  (** Type of data carried by a neutral event. *)
  type neutral

  (** Type of data carried by visible moves of the game G. *)
  type visible

  (** The type of different players. Typically, there are two players, but
      nothing forces this. *)
  module Player : Set.OrderedType
end

(** This functor takes a signature, and a implementation of of the monad, and
    returns a new implementation of the monad which supports playing and
    receiving moves, as well as basic primitives. *)

module Make (Sig : Signature) : sig
  (** Module describing labels of messages transfered. *)
  module Label : sig
    (** Type of the labels of the events. It is parametrised by ['a] which
        represents the kind of event: if 'a = `Visible, then this is a
        visible move played on the universal game G, if 'a = `Neutral, then
        it is a neutral event. *)
    type 'a _t =
      { id: int  (** Id of the move. *)
      ; desc: 'a  (** Description of the label *)
      ; player: Sig.Player.t  (** Player who emitted the move *) }

    (** Description of visible moves *)
    and visible_desc =
      { pointer: visible option
            (** A pointer, which is an optional visible move *)
      ; visible: Sig.visible
            (** The content of the move, according to {!Sig} *)
      ; address: Sig.address
            (** Where the move was played in G/pointer (or G if pointer is
                None) *) }

    (** Description of neutral events *)
    and neutral_desc = Sig.neutral

    and visible = [`Visible of visible_desc] _t

    and neutral = [`Neutral of neutral_desc] _t

    and t = [`Visible of visible_desc | `Neutral of neutral_desc] _t

    val player :
         [< `Visible of visible_desc | `Neutral of neutral_desc] _t
      -> Sig.Player.t
    (** Returns the player who emitted an event *)

    val address : visible -> Sig.address
    (** Returns the address of a visible move *)

    val pointer : visible -> visible option
    (** Returns the pointer of a visible move. *)

    val data : visible -> Sig.visible
    (** Returns data of a visible move *)

    type label = t

    val id : (label -> int) option
  end

  (** {1 The monad for describing strategies} *)

  (** We now define an extension of {!Monad.T} with primitives for playing
      moves and emitting neutral events. We first start by defining the
      signature of this monad, and then define its implementation on top of a
      regular monad. *)

  module type GamingMonad = sig
    include Monad.T

    val play :
         ?pointer:Label.visible
      -> Sig.Player.t
      -> Sig.address
      -> Sig.visible
      -> Label.visible t
    (** [play ?pointer player data] plays a new move whose pointer is
        [pointer], played by [player] and with data [data], at [address] *)

    val wait : ?pointer:Label.visible -> Sig.address -> Label.visible t
    (** [wait ?pointer address] waits for a successor to [pointer] (or
        minimal movei f [pointer] is not defined, at address [address] *)

    val neutral : Sig.Player.t -> Sig.neutral -> Label.neutral t
    (** [neutral player neutral] emits a neutral event. *)

    val project : 'a ComputationState.t -> ES.Make(Label).t
    (** Projects a computation state to its event structure. *)

    val view : ES.Make(Label).Event.View.t World.id
    (** The ID for the view *)
  end

  (** Implement a gaming monad on top of an existing causality monad, plus a
      function to display labels as string. *)
  module Monad (S : sig
    val to_string : Label.t -> string
  end)
  (M : Monad.T) : GamingMonad with type 'a t = 'a M.t
end
