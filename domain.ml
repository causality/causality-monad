module Message = struct
  type 'a t = {at: 'a Location.t; world: World.t; payload: 'a}

  let show {at; payload; world} =
    Printf.sprintf "{%s @ %s [%s]}"
      (Location.show_payload at payload)
      (Location.show at) (World.show world)

  let show_payload {at; payload; _} = Location.show_payload at payload

  let payload {payload; _} = payload

  module Filter = struct
    type 'a msg = 'a t

    type +'b t = {filter: 'a. 'a msg -> 'b option}

    let rec exec msg = function
      | [] -> None
      | {filter} :: rest -> (
        match filter msg with Some x -> Some x | None -> exec msg rest )

    let family (type value address)
        (family : (value, address) Location.family) : (value * address) t =
      { filter=
          (fun msg ->
            match Location.cast_family family msg.at with
            | None -> None
            | Some (address, cast) -> Some (cast msg.payload, address)) }

    let location l =
      { filter=
          (fun msg ->
            match Location.cast l msg.at with
            | None -> None
            | Some cast -> Some (cast msg.payload)) }

    let show = {filter= (fun msg -> Some (show msg))}

    let map {filter} f = {filter= (fun msg -> Option.map f (filter msg))}

    let ( %> ) = map
  end
end

module Value = struct
  type ('a, 'r) t = 'a Message.t
end

module PM = PolyMap.MakeRel (Location) (Value)

type t = unit PM.t

let bottom = PM.empty

let union = PM.union

let singleton msg = PM.singleton msg.Message.at msg

let add msg c = PM.add msg.Message.at msg c

let ( +++ ) t u = PM.(t +++ u)

let _match d loc = PM.lookup loc d

let payloads d loc = List.map Message.payload (_match d loc)

let dump d =
  String.concat "\n"
  @@ PM.fold (fun l (PM.B (_, msg)) -> Message.show msg :: l) [] d

let is_empty = PM.is_empty
