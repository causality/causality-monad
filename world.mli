(** World: Signature for possible worlds *)

(** In traditional models of nondeterminism, each execution of a
    nondeterministic program is looked at independently. In event structures
    models, nondeterminism is local, through a notion of conflict. Different
    nondeterministic choices can occur in different locations, and are thus
    independent, as in the program [coin || coin]. Each resolution of coin is
    only aware of its own resolution, and not of the other. Such awareness of
    which nondeterministic choices have been resolved (and how) is what we
    call a world. There are different possible implementations for worlds,
    and we give in this model the requirement on the implementation.

    Intuitively, it should be a partial order (world can be compared: w ≤
    w' if w' has seen more than w) with a bottom element (the empty world).
    However, we have found no need in the implementation yet to compare them,
    but make frequent use of the derived `join` operation. This join is
    partial: if two worlds are compatible, it returns their 'union', and if
    they are not compatible (because they have seen different resolutions of
    the same nondeterministic choices) returns nothing.*)

(** This module defines a generic world, which consists in an aggregation of
    smaller worlds. This allows several datatypes to store their view of the
    history inside a common global world. For instance, each reference can
    store the history of transactions seen. Join is then defined when all the
    components are compatible.

    Concretely, this is implemented using {!Key} and {!Treasure}. *)

(** The type of generic worlds, to which instances of {!Interface.t} can be
    added *)
type t

val show : t -> string

val join : t -> t -> t option
(** Join two worlds *)

val empty : t
(** The empty world *)

(** When inserting a new component into a generic world, then the module gets
    back an ID that it can use to later update that particular component.
    Since different components may have different types for the world,
    ['a id] is parametrised over the type of the world. *)
type 'a id

val insert :
     ?show:('a -> string)
  -> join:('a -> 'a -> 'a option)
  -> 'a
  -> t
  -> t * 'a id
(** Inserts a new component in the generic world.
    [insert ~join initial state] inserts a new world with initial state
    [initial] in [state]. The [join] function is used to compute the joins of
    this component of the world. The new world is returned, along with an id
    used to update the value of that component (see {!query}). The optional
    argument [show] is for debugging purpose. *)

val create_id :
  ?show:('a -> string) -> join:('a -> 'a -> 'a option) -> unit -> 'a id

val query : 'a id -> ('a option -> 'a option * 'b) -> t -> t * 'b
(** Generic query mechanism: [query id f world] retrieves the value of the
    component [id] (obtained from {!insert} in [world] and pass it to [f].
    [f] then must return a new value for that id along with a result. The
    call returns the updated world along with the result of [f]. This can
    easily simulate get/set using specific query functions. *)

val get : default:'a -> 'a id -> t -> 'a

val get_opt : 'a id -> t -> 'a option

val compatible : t -> t -> bool
(** Returns whether two worlds are compatible *)
