module Make (Monad : Monad.T) = struct
  (* Implementation of CCS channels. Each channel carries a counter that is
     used to generate a unique ID for each sender and each receiver.

     Moreover, each channel induces a specific world, which is a partial
     bijection on ℕ. It maps sender-ids to receiver-ids: if φ(n) = m, it
     means that receiver [m] received messaged [n]. The join is simply the
     union (not always defined). *)

  type 'a t =
    {id: int ref; seen: (int * int) list World.id; loc: (int * 'a) Location.t}

  let create ?(show = fun _ -> "") ?name () =
    (* This join simply computes the union of two partial bijections
       represented as lists of pairs *)
    let rec join m = function
      | [] -> Some m
      | m' when m = [] -> Some m'
      | (e1, e2) :: rest ->
          let ( <=> ) b b' = if b then b' else not b' in
          if List.for_all (fun (e1', e2') -> e1 = e1' <=> (e2 = e2')) m then
            join ((e1, e2) :: m) rest
          else None
    in
    let show_world m =
      let show_binding (a, b) = Printf.sprintf "%d seen by %d" a b in
      "[" ^ (String.concat "; " @@ List.map show_binding m) ^ "]"
    in
    { id= ref 0
    ; seen= World.create_id ~show:show_world ~join ()
    ; loc= Location.create ~show:(fun (_, v) -> show v) ?name () }

  (* Sending is generating a sender id, and sending it along with the value. *)
  let send channel value =
    let id = incr channel.id ; !(channel.id) in
    Monad.put channel.loc (id, value)

  let recv channel =
    (* We generate a receiver ID *)
    let self = incr channel.id ; !(channel.id) in
    let open Monad in
    (* Listen on the channel *)
    watch channel.loc
    >>= fun (id, value) ->
    (* Received message with ID [id]. We check if in our world view this
       message has been received by someone else by checking the bijection *)
    get_world_def channel.seen []
    >>= fun map ->
    (* If so, we do not do anything *)
    if List.mem_assoc id map then discard
      (* If not, then we mark that we have seen this message, and return the
         value. Future computations will not be able to receive this message. *)
    else set_world channel.seen ((id, self) :: map) >> return value

  let filter channel = Domain.Message.Filter.(location channel.loc %> snd)

  let filter_debug channel = Domain.Message.Filter.location channel.loc
end
