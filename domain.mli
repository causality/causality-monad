(** The domain of set of messages *)

(** We define here a very simple domain: 𝒫(X) where X is the type of
    message. This domain has a very simple structure: for instance it has all
    joins, and the order of primes (singleton sets) is trivial. *)

(** We start by defining messages *)
module Message : sig
  (** A message carrying a payload of type ['a] *)
  type 'a t =
    { at: 'a Location.t  (** Location where the message has been delivered *)
    ; world: World.t
          (** State of the world at the moment where the message was emitted.
              This tracks which nondceterministic choices have been seen by
              the message *)
    ; payload: 'a  (** Data of the message *) }

  val show : 'a t -> string
  (** Display a message *)

  val show_payload : 'a t -> string
  (** Show the payload of a message. *)

  (** This module defines filters on messages *)
  module Filter : sig
    type 'a msg = 'a t

    (** A filter is a function takes any messages and returns a ['a option]
        indicating whether the message matches with the filter and what is
        the match value *)
    type +'a t

    val family : ('value, 'address) Location.family -> ('value * 'address) t
    (** A filter that returns the value and the address of messages on a
        particular family. *)

    val location : 'value Location.t -> 'value t
    (** A filter that returns the value of messages on a particular location *)

    val show : string t
    (** A filter that matches all messages with its string representation. *)

    val exec : _ msg -> 'a t list -> 'a option
    (** Apply in sequence a list of filter and return the result of the first
        match, or None if there are no match *)

    val map : 'a t -> ('a -> 'b) -> 'b t
    (** Apply a function to the outcome of a filter. *)

    val ( %> ) : 'a t -> ('a -> 'b) -> 'b t
    (** Alias for {!map} *)
  end
end

(** This module is built to be an argument of {!PolyMap.Make}*)
module Value : sig
  type ('a, 'r) t = 'a Message.t
end

(** This defines the type of our domain elements. Mathematically, they are
    sets of messages, however, for implementation reasons we represent thoses
    sets as maps, mapping locations to a list of messages. The domain has all
    joins of course, and primes are singleton messages *)
type t = unit PolyMap.MakeRel(Location)(Value).t

val bottom : t
(** The empty map, representing the empty set of messages *)

val singleton : 'a Message.t -> t
(** [singleton msg] is the domain element containing only [msg] *)

val add : 'a Message.t -> t -> t
(** [add msg t] adds a message to a domain element. *)

val ( +++ ) : t -> t -> t
(** Union of two domain elements: as sets this is the literal union *)

val union : t list -> t
(** Union of a list of domain elements *)

val _match : t -> 'a Location.t -> 'a Message.t list
(** [_match d loc] returns all the messages in [d] at location [loc]. *)

val payloads : t -> 'a Location.t -> 'a list
(** [_match d loc] returns the payloads of all messages in [d] at location
    [loc]. *)

val dump : t -> string

val is_empty : t -> bool
(** [is_empty d] returns whether domain element [d] is empty *)
