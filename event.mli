(** Module for describing events.

    We describe here the events, which are the consistuent elements of an
    event structure. Events can exist independently of an event structure,
    and causality as well as conflict can be decided without neeeding an
    ambient event structure. In that sense, this presentation differs from
    the mathematical one where causality and conflict is part of the event
    structure. *)

(** Due to the Set module of the standard lib not being polymorphic, we need
    to have a functor over the type of labels. This could be fixed in the
    future by implementing a polymorphic Set type. For now, we keep this
    functorial interface as it does not create so much overhead. Hopefully,
    the user does not have directly to use the functor, but the interface in
    {!ES}. *)

(** As always, when working with functors, it is more conveninent to define
    the signature of the target module. *)

module type EventSig = sig
  (** Signature module for events (and views) *)

  (** Type of labels of events *)
  type label

  (** Type of events carrying label of type {!label} *)
  type t

  (** {2 Accessors} *)

  val id : t -> int
  (** Returns the ID of an event. *)

  val world : t -> World.t
  (** Returns the world in which the event was emitted. *)

  val label : t -> label
  (** Returns the label of an event *)

  (** {2 Views} *)

  (** Views represent configurations of events, ie. downclosed sets of
      events. They are used in particular to represent the causal history of
      an event. *)

  module View : sig
    (** The type of views, which represent causal histories. It can be
        thought of as the set of events that have been seen in the current
        context. *)
    type t

    val empty : t
    (** The empty view *)

    val world : t -> World.t

    val union : t -> t -> t
    (** Returns the union of two views. *)

    val join : t -> t -> t option
    (** Returns the join of two views, which is always defined. *)

    val id : t World.id
    (** ID for inserting a view in the world. *)
  end

  val view : t -> View.t
  (** Returns the view of an event, which represents its causal history. Note
      that [view e] does not include [e]. *)

  val prime_view : t -> View.t
  (** Returns the prime view of an event, defined as [prime_view e] = \{e\}
      \cup [view e]. *)

  val view_id : View.t World.id
  (** World id for the view *)

  val create : id:int -> world:World.t -> label:label -> t
  (** [create ~id ~world ~label] creates a new event. The view is got from
      [world]. *)

  (** {2 Relations on event} *)

  val leq : t -> t -> bool
  (** [leq e e'] returns true if [e ≤ e'] which is defined as [e] occurring
      in the view of [e']. This is fast. *)

  val predecessor : t -> t -> bool
  (** [predecessor e e'] returns true if [e] is an immediate predecessor of
      [e']. This is fast. *)

  val conflict : t -> t -> bool
  (** [conflict e e'] returns true if [e] and [e'] are in conflict, which
      here means that their worlds are not compatible. *)

  val minimal_conflict : t -> t -> bool
  (** [minimal_conflict e e'] returns true if [e] and [e'] are in minimal
      conflict, ie. they are in conflict and there are no smaller conflict.
      This is reasonably fast and does not depend on the size of the causal
      history of [e] and [e']. *)

  module EventSet : Set.S with type elt = t

  val predecessors : t -> EventSet.t
  (** Returns the set of immediate predecessors of an event. *)

  val causes : t -> EventSet.t
  (** Returns all the causes of an event *)
end

(** Make an event module out of a type of labels. *)
module Make (S : sig
  type label
end) : EventSig with type label = S.label
