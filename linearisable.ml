module type Datatype = sig
  type ('a, 'b) operation

  type 'b state

  val meaning : ('a, 'b) operation -> 'a state -> 'a state * 'b

  val show_op :
    ('a -> string) -> ('a, 'b) operation -> string * ('b -> string)

  val show_state : ('a -> string) -> 'a state -> string
end

module Transaction = struct
  type 'state t =
    | E :
        { name: string
        ; operation: string
        ; input: 'state
        ; output: 'state
        ; number: int
        ; show_result: 'result -> string
        ; id: int
        ; result: 'result }
        -> 'state t

  let id (E {id; _}) = id

  let compare t u = Int.compare (id t) (id u)

  let show show_state
      (E {name; operation; input; output; show_result; result; _}) =
    let res = show_result result in
    Printf.sprintf "[%s%s@%s:%s--->%s]" operation
      (if res = "" then "" else "(=" ^ res ^ ")")
      name (show_state input) (show_state output)
end

module Trace = struct
  type 'state t = 'state Transaction.t list

  let length = function
    | [] -> 0
    | Transaction.E {number; _} :: _ -> number + 1

  let show show_state t =
    let ts = List.map (Transaction.show show_state) @@ List.rev t in
    "[" ^ String.concat "; " ts ^ "]"

  let ( == ) t u =
    let rec aux = function
      | [], [] -> true
      | t :: q, t' :: q' ->
          if Transaction.id t = Transaction.id t' then aux (q, q') else false
      | _ -> false
    in
    aux (t, u)

  let rec drop n = function
    | l when n = 0 -> l
    | [] -> []
    | _ :: q -> drop (n - 1) q

  let is_prefix t u =
    match (t, u) with
    | [], _ -> true
    | _, [] -> false
    | l, l' ->
        let n = length l and n' = length l' in
        if n > n' then false else drop (n' - n) l' == l

  let join t u =
    let len_t = length t and len_u = length u in
    if len_t <= len_u then (drop (len_u - len_t) u == t, `Right)
    else (drop (len_t - len_u) t == u, `Left)

  let mem id = List.exists (fun t -> Transaction.id t = id)

  let empty = []

  let append trace transaction = transaction :: trace
end

type 'state world = {value: 'state; trace: 'state Trace.t}

let join s1 s2 =
  match Trace.join s1.trace s2.trace with
  | true, `Left -> Some s1
  | true, `Right -> Some s2
  | _ -> None

module SI = Set.Make (Int)

type 'state t =
  { id: 'state world World.id
  ; default: 'state
  ; show: 'state -> string
  ; name: string
  ; loc: ('state Transaction.t * 'state Trace.t) Location.t }

module Make (Monad : Monad.T) = struct
  let create ?(show = fun _ -> "") ?name default =
    { default
    ; show
    ; name= Option.value ~default:"" name
    ; loc=
        Location.create
          ~show:(fun (t, _) -> Transaction.show show t)
          ?name ()
    ; id=
        World.create_id
          ~show:(fun {trace; _} -> Trace.show show trace)
          ~join () }

  let r = ref 0

  let get cell =
    Monad.get_world_def cell.id {value= cell.default; trace= Trace.empty}

  let perform ?(name = "") ?(show_result = fun _ -> "")
      ?(act = fun _ -> Monad.return ()) ?(conflict = fun _ -> true) cell
      operation =
    let open Monad in
    let run id {value; trace} =
      operation value
      >>= fun (value', result) ->
      let entry =
        Transaction.E
          { name= cell.name
          ; operation= name
          ; input= value
          ; number= List.length trace
          ; output= value'
          ; result
          ; id
          ; show_result }
      in
      let trace = Trace.append trace entry in
      set_world cell.id {trace; value= value'}
      >> act result
      >> put cell.loc (entry, trace)
      >> return result
    in
    get cell
    >>= fun {value; trace} ->
    let id = incr r ; !r in
    let wait =
      watch cell.loc
      >>= fun (entry, previous) ->
      if
        Trace.mem id previous
        || Trace.is_prefix previous trace
        || (not @@ conflict entry)
      then discard
      else get cell >>= run id
    in
    parallel [run id {value; trace}; wait]

  let show_trace f t =
    Monad.(
      get t
      >>= fun x ->
      return x.trace
      >|= fun x -> Printf.sprintf "Trace: %s\n%!" (Trace.show f x))

  let filter {loc; _} = Domain.Message.Filter.(location loc %> fst)
end

module Cell (Monad : Monad.T) = struct
  module P = Make (Monad)

  type nonrec 'a t = 'a t

  let create = P.create

  let get ?act c =
    P.perform ?act ~name:"get"
      ~conflict:(fun (E {name; _}) -> name <> "get")
      ~show_result:c.show c
      (fun state -> Monad.return (state, state))

  let set ?(act = fun _ -> Monad.return ()) c x =
    P.perform
      ~name:(Printf.sprintf "set(%s)" (c.show x))
      ~act c
      (fun _ -> Monad.return (x, ()))

  let update ?name ?(show = fun _ -> "") ?act c f =
    P.perform ?act ?name ~show_result:show c (fun x -> Monad.return (f x))

  let filter = P.filter
end

module Lock (Monad : Monad.T) = struct
  module Cell = Cell (Monad)

  type data = Locked of unit Location.t list | Unlocked

  type t = data Cell.t

  let create ?name () =
    let show = function
      | Unlocked -> "Unlocked"
      | Locked l -> Printf.sprintf "Locked(%d)" (List.length l)
    in
    Cell.create ~show ?name Unlocked

  let lock lock =
    let open Monad in
    let a = Location.create () in
    Cell.update ~show:Bool.to_string ~name:"lock" lock (function
      | Unlocked -> (Locked [], true)
      | Locked l -> (Locked (l @ [a]), false) )
    >>= fun b -> if b then return () else watch a

  let unlock lock =
    let open Monad in
    let show = function None -> "no pending" | Some _ -> "some pending" in
    Cell.update ~name:"unlock" ~show lock (function
      | Unlocked -> assert false
      | Locked [] -> (Unlocked, None)
      | Locked (t :: rest) -> (Locked rest, Some t) )
    >>= function
    | None -> return () | Some x -> parallel [put x (); return ()]
end
