(* Instead of integers, to be able to handle families, we use a more
   sophisticated implementation. We use first-class modules and open types,
   which are merely glorified counters. *)

(* This open type contains the names of all declared channels. This is meant
   to be a gadt, where ['value] is a phantom type indicating the type of
   values the mailbox supports. Each constructor will be a family. *)
type 'value name = ..

(* This module encapsulates all the constructors added to name, ie. all the
   family. *)
module type Constructor = sig
  (** The value carried by the messages at this location *)
  type value

  (** The addressing type (used when allocating infinite families *)
  type address

  val name : string
  (** The name of the channel *)

  val show_address : address -> string
  (** A function to show a value of a particular address *)

  val show_value : value -> string
  (** A function to show values *)

  type 'value name += Loc : address -> value name

  val test : address -> 'value name -> (value, 'value) PolyMap.compare
  (** [test address name] tests whether [Loc address] denotes the same
      channel as [name]. *)
end

(** The module type reified into a parametrised type *)
type ('value, 'address) family =
  (module Constructor with type value = 'value and type address = 'address)

(* Create a new constructor, ie. build a module of signature {!Consturctor}. *)
let create_family (type value address) ?(show_value = fun _ -> "")
    ?(show_address = fun _ -> "") ?(name = "")
    (compare : address -> address -> int) : (value, address) family =
  ( module struct
    type nonrec value = value

    type nonrec address = address

    let name = name

    let show_value = show_value

    let show_address = show_address

    type 'value name += Loc : address -> value name

    let test (type a) address : a name -> (value, a) PolyMap.compare =
      function
      | Loc address' -> (
        match compare address address' with
        | 0 -> PolyMap.Equal
        | n when n > 0 -> PolyMap.Greater
        | _ -> PolyMap.Less )
      | x ->
          (* I would like to compare the tags of the internal representation
             but somehow sometimes the tags can be identical without the
             match to go through the first case. Is it because some
             constructors are boxed and some are not? *)
          if Obj.(Stdlib.compare (repr (Loc address)) (repr x)) < 0 then
            PolyMap.Less
          else PolyMap.Greater
  end )

(* The type of actual locations is obtained by existential quantification
   over constructor to make the inner type of indices disappear *)
type 'value t = Chan : ('value, 'address) family * 'address -> 'value t

let at family x = Chan (family, x)

let create ?show ?name () =
  at (create_family ?show_value:show ?name Unit.compare) ()

let show (type a) (Chan (constructor, address) : a t) =
  let module M = (val constructor) in
  match M.show_address address with
  | "" -> M.name
  | s -> Printf.sprintf "%s(%s)" M.name s

let show_payload (type a) (Chan (constructor, _) : a t) =
  let module M = (val constructor) in
  M.show_value

let compare (type a b) (Chan (c1, a1) : a t) (Chan (c2, a2) : b t) :
    (a, b) PolyMap.compare =
  let module C1 = (val c1) in
  let module C2 = (val c2) in
  C1.test a1 (C2.Loc a2)

let cast_family (type a b c) (family : (a, b) family)
    (Chan (fam', address) : c t) : (b * (c -> a)) option =
  let module C1 = (val family) in
  let module C2 = (val fam') in
  match C2.Loc address with C1.Loc a -> Some (a, fun x -> x) | _ -> None

let cast (Chan (fam, _)) loc =
  match cast_family fam loc with None -> None | Some (_, f) -> Some f
