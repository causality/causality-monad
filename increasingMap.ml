(** The type of increasing maps on D *)
module MapHandler =
  PolyMap.MakeRel
    (Location)
    (struct
      type ('a, 'r) t = 'a Domain.Message.t -> 'r option
    end)

type t = {init: Domain.t; handlers: t MapHandler.t}

let empty = {init= Domain.bottom; handlers= MapHandler.empty}

let is_empty {init; handlers} =
  Domain.is_empty init && MapHandler.is_empty handlers

let ( +++ ) t u =
  { init= Domain.(t.init +++ u.init)
  ; handlers= MapHandler.(t.handlers +++ u.handlers) }

let dump {init; handlers} =
  Printf.sprintf "- init=%s\n- handlers=%s" (Domain.dump init)
    (MapHandler.fold
       (fun s (MapHandler.B (loc, _)) ->
         Printf.sprintf "%s,%s" (Location.show loc) s )
       "" handlers )

let union = function [] -> empty | t :: q -> List.fold_left ( +++ ) t q

let emit msg = {init= Domain.singleton msg; handlers= MapHandler.empty}

let watch location handler =
  {init= Domain.bottom; handlers= MapHandler.singleton location handler}

let init {init; _} = init

module Iteration = struct
  (* We now show how to compute the closure of an increasing map using this
     representation. *)

  (* We then define a transition system over states which are pairs of
     increasing map. The first component, closed, represent the part
     of the computation that has already been closed, and the second
     part is the new threads to incorporate. The invariant is that new
     threads arising from the closed (ie. when feeding messages of
     closed to itself) should be included in [closed +++ to_add]. *)

  type state = {closed: t; to_add: t}

  (* [sender @-> receiver] computes the computation resulting in feeding the messages of [sender] to the handler of [receiver]. *)
  let ( @-> ) sender receiver : t =
    let fold acc (MapHandler.B (loc, handler)) =
      let msgs = Domain._match sender.init loc in
      let threads = List.filter_map handler msgs in
      if threads <> [] then union (acc :: threads) else acc
    in
    MapHandler.fold fold empty receiver.handlers

  (* We then define the transition system, by incorporating to_add in closed *)
  let rec normalise {closed; to_add} =
    if is_empty to_add then closed
    else
      let x = closed +++ to_add in
      normalise {closed= x; to_add= (to_add @-> x) +++ (closed @-> to_add)}
end

let add_and_close t u =
  let open Iteration in
  normalise {closed= t; to_add= u}

let close t = add_and_close empty t
