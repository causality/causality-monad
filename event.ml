module type EventSig = sig
  (** Type of labels of events *)
  type label

  (** Type of events carrying label of type {!label} *)
  type t

  (** {2 Accessors} *)

  val id : t -> int
  (** Returns the ID of an event. *)

  val world : t -> World.t
  (** Returns the world in which the event was emitted. *)

  val label : t -> label
  (** Returns the label of an event *)

  (** {2 Views} *)
  module View : sig
    (** The type of views, which represent causal histories. It can be
        thought of as the set of events that have been seen in the current
        context. *)
    type t

    val empty : t

    val world : t -> World.t

    val union : t -> t -> t
    (** Returns the union of two views. *)

    val join : t -> t -> t option
    (** Returns the join of two views, which is always defined. *)

    val id : t World.id
    (** ID for inserting a view in the world. *)
  end

  val view : t -> View.t
  (** Returns the view of an event, which represents its causal history. Note
      that [view e] does not include [e]. *)

  val prime_view : t -> View.t
  (** Returns the prime view of an event, which is itself together with its
      view. *)

  val view_id : View.t World.id

  val create : id:int -> world:World.t -> label:label -> t
  (** [create ~id ~world ~label] creates a new event. The view is got from
      [world]. *)

  (** {2 Relations on event} *)

  val leq : t -> t -> bool
  (** [leq e e'] returns true if [e ≤ e'] which is defined as [e] occurring
      in the view of [e']. This is fast. *)

  val predecessor : t -> t -> bool
  (** [predecessor e e'] returns true if [e] is an immediate predecessor of
      [e']. This is fast. *)

  val conflict : t -> t -> bool
  (** [conflict e e'] returns true if [e] and [e'] are in conflict, which
      here means that their worlds are not compatible. *)

  val minimal_conflict : t -> t -> bool
  (** [minimal_conflict e e'] returns true if [e] and [e'] are in minimal
      conflict, ie. they are in conflict and there are no smaller conflict.
      This is reasonably fast and does not depend on the size of the causal
      history of [e] and [e']. *)

  module EventSet : Set.S with type elt = t

  val predecessors : t -> EventSet.t

  val causes : t -> EventSet.t
end

module Make (S : sig
  type label
end) =
struct
  module rec Event : sig
    type t =
      { view: V.t
      ; world: World.t
      ; label: S.label
      ; id: int
      ; world_before: World.t }

    val compare : t -> t -> int
  end = struct
    type t =
      { view: V.t
      ; world: World.t
      ; label: S.label
      ; id: int
      ; world_before: World.t }

    let compare {id; _} {id= id'; _} = Int.compare id id'
  end

  and EventSet : (Set.S with type elt = Event.t) = Set.Make (Event)

  and V : sig
    type t = {downclosure: EventSet.t; predecessors: EventSet.t}
  end = struct
    type t = {downclosure: EventSet.t; predecessors: EventSet.t}
  end

  type label = S.label

  type t = Event.t

  let label {Event.label; _} = label

  let world {Event.world; _} = world

  let id {Event.id; _} = id

  let view {Event.view; _} = view

  module View = struct
    include V

    let empty = {predecessors= EventSet.empty; downclosure= EventSet.empty}

    let world {V.predecessors; _} =
      EventSet.fold
        (fun event world ->
          match World.join event.world world with
          | Some w -> w
          | _ -> failwith "Incompatible worlds in a view.")
        predecessors World.empty

    let merge ( ++ ) ( -- ) (a, b) (c, d) = a -- (d -- c) ++ (c -- (b -- a))

    let union v1 v2 =
      { downclosure= EventSet.union v1.downclosure v2.downclosure
      ; predecessors=
          merge EventSet.union EventSet.diff
            (v1.predecessors, v1.downclosure)
            (v2.predecessors, v2.downclosure) }

    let join v1 v2 = Some (union v1 v2)

    let id = World.create_id ~join ()
  end

  let view_id = World.create_id ~join:View.join ()

  let create ~id ~world ~label =
    let view = World.get ~default:View.empty view_id world in
    {Event.id; world; view; label; world_before= View.world view}

  let leq e e' = EventSet.mem e e'.Event.view.downclosure

  let predecessor e e' = EventSet.mem e e'.Event.view.predecessors

  let prime_view e =
    { V.predecessors= EventSet.singleton e
    ; downclosure= EventSet.add e e.view.downclosure }

  let conflict e e' = not @@ World.compatible e.Event.world e'.Event.world

  let minimal_conflict e e' =
    conflict e e'
    && World.compatible e.Event.world e'.Event.world_before
    && World.compatible e'.Event.world e.Event.world_before

  let causes e = e.Event.view.downclosure

  let predecessors e = e.Event.view.predecessors
end
