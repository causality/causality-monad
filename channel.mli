(** CCS/π channels *)

(** This module implements asynchronous regular channels, as can be found in
    process calculi as CCS or π: messages can be sent and received on
    channels, and each message is received at most once: receiving "consumes"
    the message and makes it invisible for other receivers.

    These channels are asynchronous: sending returns right away. *)

module Make (Monad : Monad.T) : sig
  (** The type of channels carrying values of type ['a] *)
  type 'a t

  val create : ?show:('a -> string) -> ?name:string -> unit -> 'a t
  (** Creating a new channel. [show] and [name] are used for debugging
      purposes. *)

  val send : 'a t -> 'a -> unit Monad.t
  (** [send c v] sends value [v] on channel [c] *)

  val recv : 'a t -> 'a Monad.t
  (** [recv c] receives (and returns) a message on [c]. [recv c] is
      guaranteed to return once in each execution *)

  val filter : 'a t -> 'a Domain.Message.Filter.t
  (** [filter c] returns the filter on [c]. *)

  val filter_debug : 'a t -> (int * 'a) Domain.Message.Filter.t
  (** [filter_debug c] returns a filter that also exposes the ID of the
      message. *)
end
