(* % Causal interpreter for shared-memory concurrency (SC) *)

(* In this tutorial, we see how to write an interpreter for a simple
   imperative concurrent language with shared-memory concurrency using
   `Causality`. We first focus on computing the final states of a given
   programs, and then show how we can a more apt description of the
   operational behaviour by generating an *event structure* which represents
   all the actions a program may do. *)

(* # Syntax of the source language

   We consider here a simple concurrent imperative language where threads
   communicate through the memory. The memory is addressed by integer, and
   each thread has access to local registers. *)

type address = int

type register = string

(* Programs are lists of threads to be run in parallel. *)

type program = thread list

(* Thread are sequential units. We consider here a simple language with
   sequential composition, conditionals and basic actions. *)
and thread =
  | Seq of thread * thread
  | If of bool_expr * thread * thread
  | Instruction of instruction
  | Noop

(* The instruction of our languages are simply read and writes to locations *)
and instruction =
  | Load of address * register
  | Store of address * arith_expr

(* We then define basic expressions manipulating register *)
and arith_expr =
  | Int of int
  | Reg of register
  | Add of arith_expr * arith_expr

and bool_expr = IsZero of arith_expr

(* # Configuring the causality module *)

(* We now get ready to define our interpreter of this language. For that, we
   need to instanciated some parameters in the Causality library. *)

open Causality

(* To instanciate the library, we need to provide the type for labels carried
   by events. In our case, events correspond to memory operations: read and
   writes, that we define now. *)

module Label = struct
  type mode = Read | Write

  type label = {mode: mode; addr: address; value: int}

  let show {mode; addr; value} =
    Printf.sprintf "%s[%d]=%d" (if mode = Read then "R" else "W") addr value

  let read addr value = {addr; value; mode= Read}

  let write addr value = {addr; value; mode= Write}
end

(* We now instanciate the Event Structure module of Causality with this Label
   module. The optional id parameter is for more advanced usages. *)
module ES = ES.Make (struct
  include Label

  let id = None
end)

(* The module ES provides us with a Monad that we will make use of for our
   interpreters. This monad supports concurrent operations plus emitting
   event in the event structure. *)

open ES.Monad

let ( let* ) = bind

(* We can now start writing our interpreter. We start with expressions. *)

let rec eval_aexpr (env : (register * int) list) = function
  | Int n -> n
  | Reg v -> ( try List.assoc v env with _ -> 0 )
  | Add (e, e') -> eval_aexpr env e + eval_aexpr env e'

let eval_bexpr env (IsZero a) = eval_aexpr env a = 0

(* We need now to evaluate the instructions, where the fun begins. For these,
   we make use of the `Cell` module of `Causality` that implements a
   concurrent reference. *)

(* The Cell module needs to be configured with the particular monad we are
   interested in. *)
module Cell = Linearisable.Cell (ES.Monad)

(* To evaluate instructions, we need a memory, that is an assignement of
   addresses to cells. *)
type memory = int Cell.t array

let eval_instr memory env = function
  | Load (address, register) ->
      (* In the case of a load, we query the value of the memory at the given
         address. We use the optional paramater `act` which allows us to
         execute something at the instant when the read is actually performed
         by the cell. In this case, we `emit` an event in our event structure
         with the proper label. We then return the updated environment. *)
      let* n =
        Cell.get ~act:(fun v -> emit (Label.read address v)) memory.(address)
      in
      return ((register, n) :: env)
  | Store (address, expr) ->
      (* For writes, we first evaluate the expression to get the value to
         write. We then write the value and return the environment unchanged. *)
      let value = eval_aexpr env expr in
      let* () =
        Cell.set
          ~act:(fun () -> emit (Label.write address value))
          memory.(address) value
      in
      return env

(* We now move on to evaluating threads, simply by induction. *)
let rec eval_thread memory env = function
  | Instruction i -> eval_instr memory env i
  | If (b, t, u) -> eval_thread memory env (if eval_bexpr env b then t else u)
  | Seq (t, u) ->
      let* env = eval_thread memory env t in
      eval_thread memory env u
  | Noop -> return env

(* Finally, for programs, we use the join_list function that spawns a list of
   computations in parallel; wait for all of them to return and return the
   list of results. *)
let eval_program prog =
  let memory = Array.init 3 (fun _ -> Cell.create 0) in
  join_list (List.map (eval_thread memory []) prog)

(* To test this interpreter, we recreate the message-passing program. *)

let data = 0

and flag = 1

let store x y = Instruction (Store (x, y))

let load x y = Instruction (Load (x, y))

let program =
  [ Seq (store data (Int 17), store flag (Int 1))
  ; Seq
      ( load flag "r"
      , If
          ( IsZero (Add (Reg "r", Int (-1)))
          , (* tests if r = 1 *) load data "s"
          , Noop ) ) ]

(* With the following we can run the program. When running this script
   without cli arguments, we simply display the event structure as a pdf,
   otherwise we write the output as a png in the first cli argument. *)

let () =
  if Array.length Sys.argv = 1 then display Label.show (eval_program program)
  else
    Dot.to_png
      (ES.to_dot Label.show (run_es (eval_program program)))
      Sys.argv.(1)

(* The result is: *)

(* ![](examples/sc.png) *)

(* Where the race between the two accesses on the flag (address 1) are marked
   with a conflict. *)
