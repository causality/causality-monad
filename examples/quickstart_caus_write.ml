type var = string

(* Arithmetic expressions of our language *)
type expr = Int of int | Var of var | Add of expr * expr

(* The only instruction of our language *)
type instr = Write of var * expr

(* A program is a list of instruction. *)
type program = instr list

(* Setting up the library *)
open Causality

(* We instanciate the ES module (Event Structures) with the type of label for
   our events which are for now mere string. The id parameter is left to
   [None] (it is used in more advanced cases.) *)

module ES = ES.Make (struct
  type label = string

  let id = None
end)

open ES.Monad

let ( let* ) = bind

(* We can now start writing our interpreter. *)

(* We start with expressions *)
let rec eval_expr (env : (var * int t) list) = function
  | Int n -> return n
  | Var v -> List.assoc v env
  | Add (e, e') ->
      let* x = eval_expr env e in
      let* y = eval_expr env e' in
      return (x + y)

(* We continue with instructions. Each instruction will emit an event
   describing the operation. *)

(* In this version, we want to preserve the order on the writes on the same variable. For this, we will store
   a "promise table" mapping each variable to a promise. *)
type table = (var * unit Monad.t) list

(* Wait on the promise for a given variable in a table. If the table has no promise for the variable, it means that there is no previous write on that variable, hence nothing to wait on.*)

let wait_var table variable =
  try List.assoc variable table 
  with Not_found -> return ()

(* Due to the call to emit, that emits an event, the eval_instr has a monadic
   return type (`(string * int) t`) *)
let eval_instr env (Write (var, expr)) =
  let l = Location.create () in
  let env' = (var, watch l) :: env in
  let* () =
    spawn (fun () ->
        let* value = eval_expr env expr in
        (* Wait on the previous write on var, if any *)
        let* _ = try List.assoc var env with Not_found -> return 0 in
        let* () = emit (Printf.sprintf "W%s:=%d" var value) in
        put l value )
  in
  return env'

(* We can now evaluate a whole program by evaluating each instruction in
   sequence. *)
let rec eval_program ?(env = []) = function
  | [] -> return env
  | t :: q ->
      let* env = eval_instr env t in
      eval_program ~env q

let program =
  [ Write ("x", Int 1)
  ; Write ("y", Int 2)
  ; Write ("x", Int 3)
  ; Write ("z", Add (Var "x", Var "y")) ]

let () =
  if Array.length Sys.argv = 1 then
    display (fun s -> s) (eval_program program)
  else
    Dot.to_png
      (ES.to_dot (fun s -> s) (run_es (eval_program program)))
      Sys.argv.(1)
