module Attribute = struct
  type t = {name: string; value: string}

  let show {name; value} = Printf.sprintf "%s=%S" name value

  let show_list l = String.concat ", " @@ List.map show l

  let arrowhead kind = {name= "arrowhead"; value= kind}

  let fillcolor lbl = {name= "fillcolor"; value= lbl}

  let color c = {name= "color"; value= c}

  let shape c = {name= "shape"; value= c}

  let label lbl = {name= "label"; value= lbl}

  let style s = {name= "style"; value= s}

  let class_ s = {name= "class"; value= s}

  let id s = {name= "id"; value= s}
end

module Node = struct
  type t = {name: string; attrs: Attribute.t list}

  let show {name; attrs} =
    Printf.sprintf "%s [%s]" name (Attribute.show_list attrs)

  let make ~name ~attrs = {name; attrs= Attribute.id name :: attrs}

  let name {name; _} = name
end

module Edge = struct
  type t = {src: string; tgt: string; attrs: Attribute.t list}

  let show {src; tgt; attrs} =
    Printf.sprintf "%s -> %s [%s]" src tgt (Attribute.show_list attrs)

  let make ~src ~tgt ~attrs = {src; tgt; attrs}
end

type t = {nodes: Node.t list; edges: Edge.t list}

let to_string ?(name = "es") t =
  Printf.sprintf "digraph %s {\n %s\n%s\n }" name
    (String.concat "\n" @@ List.map Node.show t.nodes)
    (String.concat "\n" @@ List.map Edge.show t.edges)

let to_file t fname =
  let fd = open_out fname in
  output_string fd (to_string t) ;
  close_out fd

let convert ?(fmt = "pdf") graph output =
  let temp = Filename.temp_file "graph" "dot" in
  to_file graph temp ;
  ignore @@ Sys.command
  @@ Printf.sprintf "dot -T%s %s > %s" fmt (Filename.quote temp)
       (Filename.quote output)

let to_pdf g o = convert g o

let to_png g o = convert ~fmt:"png" g o

let view ?(viewer = "xdg-open") t =
  let pdf_file = Filename.temp_file "graph" "pdf" in
  let () = to_pdf t pdf_file in
  ignore @@ Sys.command
  @@ Printf.sprintf "%s %s" viewer (Filename.quote pdf_file)
